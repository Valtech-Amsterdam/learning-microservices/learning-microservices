﻿using FluentValidation;

namespace Data.Models
{
    public sealed class UpdateCommandValidator<TModel> : AbstractValidator<UpdateCommand<TModel>>
    {
        public UpdateCommandValidator(IValidator<TModel> dataValidator)
        {
            RuleFor(data => data)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull();

            RuleForEach(data => data.Set)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .SetValidator(dataValidator);

            RuleForEach(data => data.Delete)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
        }
    }
}
