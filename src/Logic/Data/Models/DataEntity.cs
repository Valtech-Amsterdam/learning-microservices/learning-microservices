﻿using System;

namespace Data.Models
{
    public sealed class DataEntity
    {
        public string Id { get; set; }

        public DateTime AddedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
