﻿namespace Data.Models
{
    public interface IIdentity
    {
        string Id { get; }
    }
}
