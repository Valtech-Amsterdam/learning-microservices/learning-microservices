﻿using System.Collections.Generic;
using YamlDotNet.Serialization;

namespace Data.Models
{
    public sealed class UpdateCommand<TData>
    {
        public IEnumerable<TData> Set { get; set; }

        [YamlMember(Alias = "del")]
        public IEnumerable<string> Delete { get; set; }
    }
}
