﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Models;
using FluentValidation;
using Ftp;
using ImportData;
using ImportData.Models;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using Reporting.Client;
using Formatting = Core.Constants.Formatting;

namespace Transactions.Handlers
{
    /// <inheritdoc cref="FtpToQueueHandler{TModel}"/>
    public sealed class FtpToQueueHandler<TModel> : IFtpResultHandler 
        where TModel : class, new()
    {
        private readonly TraceWriter _log;
        private readonly CloudQueue _queue;
        private readonly IValidator<TModel> _modelValidator;

        public FtpToQueueHandler(TraceWriter log, CloudQueue queue, IValidator<TModel> modelValidator)
        {
            _log = log;
            _queue = queue;
            _modelValidator = modelValidator;
        }

        public async Task HandleFtpFilesAsync(FtpConnection ftpConnection, List<string> fileNames, ReportClient reportClient)
        {
            _log.Info("Parsing and validating files");
            var parsedFiles = await FileParser<UpdateCommand<TModel>>.ParseFiles(fileNames, ftpConnection, reportClient)
                .ConfigureAwait(false);
            var dataValidator = new UpdateCommandValidator<TModel>(_modelValidator);
            var validFiles = (await DataValidator<TModel>.ValidateFiles(parsedFiles, ftpConnection, dataValidator, reportClient)
                .ConfigureAwait(false))
                .ToList();

            _log.Info($"Successfully parsed and validated {validFiles.Count} files");
            _log.Info("Pushing to queue");

            foreach (var file in validFiles)
            {
                var jsonModel = JsonConvert.SerializeObject(file.Data, Formatting.JsonSerializerSettings);
                await _queue.AddMessageAsync(new CloudQueueMessage(jsonModel)).ConfigureAwait(false);
                await ftpConnection.ApproveFileAsync(file.Name).ConfigureAwait(false);
            }
        }
    }
}
