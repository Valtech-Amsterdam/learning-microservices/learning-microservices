﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ftp;
using Microsoft.WindowsAzure.Storage.Blob;
using Reporting.Client;

namespace Transactions.Handlers
{
    /// <inheritdoc cref="FtpToBlobHandler"/>
    public sealed class FtpToBlobHandler : IFtpResultHandler
    {
        private readonly CloudBlobContainer _blobContainer;

        /// <summary>
        /// Move files from an ftp server to a blob container
        /// </summary>
        /// <param name="blobContainer"></param>
        public FtpToBlobHandler(CloudBlobContainer blobContainer)
        {
            _blobContainer = blobContainer;
        }

        public async Task HandleFtpFilesAsync(FtpConnection ftpConnection, List<string> fileNames, ReportClient reportClient)
        {
            foreach (var fileName in fileNames)
            {
                var blob = _blobContainer.GetBlockBlobReference(fileName.Replace(' ', '-').ToLower());
                using (var fileStream = await ftpConnection.OpenFileReadStreamAsync(fileName))
                    await blob.UploadFromStreamAsync(fileStream).ConfigureAwait(false);

                await ftpConnection.ApproveFileAsync(fileName).ConfigureAwait(false);
            }
        }
    }
}
