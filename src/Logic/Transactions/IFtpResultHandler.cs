﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ftp;
using Reporting.Client;

namespace Transactions
{
    public interface IFtpResultHandler
    {
        Task HandleFtpFilesAsync(FtpConnection ftpConnection, List<string> fileNames, ReportClient reportClient);
    }
}
