﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ftp;
using Microsoft.Azure.WebJobs.Host;
using Reporting.Client;

namespace Transactions
{
    /// <inheritdoc cref="FtpTransaction"/>
    public sealed class FtpTransaction
    {
        private readonly TraceWriter _log;
        private readonly ReportClient _reportClient;
        private readonly IFtpResultHandler _resultHandlerHandler;
        private readonly FtpConnectionFactory _ftpConnectionFactory;

        /// <summary>
        /// Move files from an ftp server to a blob container
        /// </summary>
        /// <param name="log"></param>
        /// <param name="reportClient"></param>
        /// <param name="resultHandlerHandler"></param>
        public FtpTransaction(TraceWriter log, ReportClient reportClient, IFtpResultHandler resultHandlerHandler)
        {
            _log = log;
            _reportClient = reportClient;
            _resultHandlerHandler = resultHandlerHandler;
            _ftpConnectionFactory = new FtpConnectionFactory();
        }

        public Task Transfer(string path = "/", string extension = null) => Transfer(path ?? "/", new[] { extension });

        /// <summary>
        /// Execute the transfer operation
        /// </summary>
        /// <returns></returns>
        public async Task Transfer(string path = "/", IEnumerable<string> extensions = null)
        {
            _log.Info("Connecting to FTP");
            using (var ftpConnection = await _ftpConnectionFactory.CreateConnection(_reportClient, path))
            {
                var files = await ftpConnection.ListFilesWithExtensionAsync(extensions).ConfigureAwait(false);
                if (files?.Any() != true)
                {
                    _log.Info("No files to import");
                    return;
                }

                _log.Info($"Importing {files.Count} files");
                
                await _resultHandlerHandler.HandleFtpFilesAsync(
                    ftpConnection, 
                    files
                        .OrderBy(file => file.Name)
                        .ThenBy(file => file.DateModified)
                        .Select(file => file.Name)
                        .ToList(), 
                    _reportClient);
            }
        }
    }
}
