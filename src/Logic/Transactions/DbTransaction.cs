﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Data.Models;
using FluentValidation;
using ImportData.Extensions;
using KeyVault;
using Microsoft.Azure.WebJobs.Host;
using MongoDB.Driver;
using Reporting.Client;

namespace Transactions
{
    /// <inheritdoc cref="DbTransaction{TModel}"/>
    public sealed class DbTransaction<TModel> where TModel : IIdentity
    {
        private readonly TraceWriter _log;
        private readonly ReportClient _reportClient;
        private readonly ConfiguredTaskAwaitable<string> _connectionString;

        /// <summary>
        /// Move ftp files to a database source
        /// </summary>
        public DbTransaction(TraceWriter log, ReportClient reportClient, string connectionStringSecret)
        {
            _log = log;
            _reportClient = reportClient;
            _connectionString = KeyVaultReader.GetSecretAsync(connectionStringSecret).ConfigureAwait(false);
        }

        /// <summary>
        /// Execute the transfer operation
        /// </summary>
        /// <returns></returns>
        public async Task Transfer(UpdateCommand<TModel> updateCommand, IValidator<TModel> validator)
        {
            var database = await GetConnection();
            if (database == null) return;
            EnsureCollectionsAsync(database);

            _log.Info("Validating queued import data");
            await Validate(updateCommand, validator);

            await PushToDatabase(updateCommand, database).ConfigureAwait(false);
        }

        private async Task Validate(UpdateCommand<TModel> updateCommand, IValidator<TModel> validator)
        {
            try
            {
                var dataValidator = new UpdateCommandValidator<TModel>(validator);
                await dataValidator.ValidateAndThrowAsync(updateCommand).ConfigureAwait(false);
            }
            catch (ValidationException ex)
            {
                await _reportClient.ReportValidationError(ex.ToReportLine());
                throw;
            }
            catch (Exception ex)
            {
                await _reportClient.ReportException(ex.ToString());
                throw;
            }
        }

        private async Task<IMongoDatabase> GetConnection()
        {
            var connectionString = await _connectionString;
            if (string.IsNullOrEmpty(connectionString))
            {
                await _reportClient.ReportException("No valid connectionString given for db transaction");
                return null;
            }
            var client = new MongoClient(connectionString);
            return client.GetDatabase(typeof(TModel).Name);
        }

        private async Task PushToDatabase(
            UpdateCommand<TModel> updateCommand,
            IMongoDatabase database)
        {
            var dataCollection = database.GetCollection<TModel>(typeof(TModel).Name);
            var entityCollection = database.GetCollection<DataEntity>(nameof(DataEntity));

            try
            {
                if (updateCommand.Set != null)
                    foreach (var setData in updateCommand.Set)
                    {
                        var now = DateTime.UtcNow;
                        var entity = new DataEntity
                        {
                            Id = setData.Id,
                            UpdatedDate = now
                        };
                        
                        var result = await dataCollection.FindOneAndReplaceAsync(item => item.Id == setData.Id, setData);
                        if (result != null)
                        {
                            await entityCollection.ReplaceOneAsync(item => item.Id == setData.Id, entity);
                            continue;
                        }

                        entity.AddedDate = now;
                        await dataCollection.InsertOneAsync(setData);
                        await entityCollection.InsertOneAsync(entity);
                    }

                if (updateCommand.Delete != null)
                    foreach (var deleteData in updateCommand.Delete)
                    {
                        await dataCollection.FindOneAndDeleteAsync(item => item.Id == deleteData);
                        await entityCollection.FindOneAndDeleteAsync(item => item.Id == deleteData);
                    }
            }
            catch (Exception ex)
            {
                await _reportClient.ReportException(ex.Message);
                throw;
            }
        }

        // Sync on purpose
        private static void EnsureCollectionsAsync(IMongoDatabase database)
        {
            try
            {
                var collectionNames = database.ListCollectionNames().ToList();

                if (!collectionNames.Contains(typeof(TModel).Name))
                    database.CreateCollection(typeof(TModel).Name);
                if (!collectionNames.Contains(nameof(DataEntity)))
                    database.CreateCollection(nameof(DataEntity));
            }
            catch
            {
                // do nothing, this fails if the collecation already got created between listing and creating
            }
        }
    }
}
