﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using Polly;
using Polly.Extensions.Http;
using Polly.Retry;
using Polly.Timeout;
using Formatting = Core.Constants.Formatting;

namespace HttpClient
{
    public class RetryingHttpClient : IDisposable
    {
        private readonly System.Net.Http.HttpClient _webClient;
        private readonly RetryPolicy<HttpResponseMessage> _retryPolicy;

        private static readonly HttpStatusCode[] HttpStatusCodesWorthRetrying = {
            HttpStatusCode.RequestTimeout, // 408
            HttpStatusCode.BadGateway, // 502
            HttpStatusCode.ServiceUnavailable, // 503
            HttpStatusCode.GatewayTimeout // 504
        };

        public RetryingHttpClient(string baseUrl = null)
        {
            _retryPolicy = HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(r => HttpStatusCodesWorthRetrying.Contains(r.StatusCode))
                .Or<TimeoutRejectedException>() // thrown by Polly's TimeoutPolicy if the inner execution times out
                .WaitAndRetryAsync(new[]
                {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(10),
                    TimeSpan.FromMinutes(10)
                });

            _webClient = new System.Net.Http.HttpClient
            {
                // ReSharper disable once AssignNullToNotNullAttribute
                BaseAddress = string.IsNullOrWhiteSpace(baseUrl) ? null : new UriBuilder(baseUrl).Uri
            };
        }

        private string Serialize<TModel>(TModel data) => JsonConvert.SerializeObject(data, Formatting.JsonSerializerSettings);
        public void Dispose() => _webClient?.Dispose();
        
        public ConfiguredTaskAwaitable<HttpResponseMessage> GetAsync(Uri url) 
            => _retryPolicy.ExecuteAsync(() =>
                _webClient.GetAsync(url)).ConfigureAwait(false);

        public ConfiguredTaskAwaitable<HttpResponseMessage> PostAsync<TModel>(string url, TModel data)
        {
            var requestBody = new StringContent(Serialize(data), Encoding.UTF8, "application/json");

            return _retryPolicy.ExecuteAsync(() =>
                _webClient.PostAsync(url, requestBody)).ConfigureAwait(false);
        }
    }
}
