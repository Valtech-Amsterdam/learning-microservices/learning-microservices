﻿namespace Ftp.Models
{
    public sealed class FileResult<TData>
    {
        public string Name { get; set; }

        public TData Data { get; set; }
    }
}
