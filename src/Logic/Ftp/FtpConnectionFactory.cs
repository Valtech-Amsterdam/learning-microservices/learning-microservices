﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using CoreFtp;
using CoreFtp.Enum;
using KeyVault;
using Reporting.Client;

namespace Ftp
{
    public sealed class FtpConnectionFactory
    {
        public async Task<FtpConnection> CreateConnection(ReportClient reportClient, string path)
        {
            try
            {
                var ftpClient = await CreateFtpClient(path);
                var ftpConnection = new FtpConnection(ftpClient);
                await ftpClient.LoginAsync().ConfigureAwait(false);

                return ftpConnection;
            }
            catch (Exception ex)
            {
                await reportClient.ReportException($"An exception occured while connecting to the FTP server, \n{ex}");
                throw;
            }
        }

        private static async Task<FtpClient> CreateFtpClient(string path)
        {
            var ftpHost = await KeyVaultReader.GetSecretAsync("Ftp-Hostname");
            var username = await KeyVaultReader.GetSecretAsync("Ftp-Username");
            var password = await KeyVaultReader.GetSecretAsync("Ftp-Password");

            var config = new FtpClientConfiguration
            {
                Host = ftpHost,
                EncryptionType = FtpEncryption.None,
                SslProtocols = SslProtocols.None,
                IgnoreCertificateErrors = true,
                Username = username,
                Password = password,
                Mode = FtpTransferMode.Ascii,
                BaseDirectory = string.IsNullOrWhiteSpace(path) ? "/" : path
            };

            var port = Environment.GetEnvironmentVariable("Ftp-Port");
            config.Port = string.IsNullOrEmpty(port) ? 21 : int.Parse(port);

            var client = new FtpClient(config);
            return client;
        }
    }
}
