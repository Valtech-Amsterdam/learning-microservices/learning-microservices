﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Constants;
using Core.Extensions;
using CoreFtp;
using CoreFtp.Infrastructure;

namespace Ftp
{
    /// <summary>
    /// Create an ftp connection that immediately logs in and reports any exceptions if they occur
    /// </summary>
    public sealed class FtpConnection : IDisposable
    {
        private readonly FtpClient _ftpClient;

        public FtpConnection(FtpClient ftpClient)
        {
            _ftpClient = ftpClient;
        }

        /// <inheritdoc />
        public async void Dispose()
        {
            try
            {
                if(_ftpClient.IsConnected)
                    await _ftpClient.LogOutAsync().ConfigureAwait(false);
            }
            finally
            {
                _ftpClient.Dispose();
            }
        }

        /// <inheritdoc cref="IFtpClient.OpenFileReadStreamAsync"/>
        public ConfiguredTaskAwaitable<Stream> OpenFileReadStreamAsync(string fileName) 
            => _ftpClient.OpenFileReadStreamAsync(fileName).ConfigureAwait(false);
        /// <inheritdoc cref="IFtpClient.OpenFileReadStreamAsync"/>
        public Stream OpenFileReadStream(string fileName)
            => _ftpClient.OpenFileReadStreamAsync(fileName).GetResultSynchronously();
        /// <inheritdoc cref="IFtpClient.ListAllAsync"/>
        public ConfiguredTaskAwaitable<ReadOnlyCollection<FtpNodeInformation>> ListAllAsync() 
            => _ftpClient.ListAllAsync().ConfigureAwait(false);
        /// <inheritdoc cref="IFtpClient.ListFilesAsync"/>
        public ConfiguredTaskAwaitable<ReadOnlyCollection<FtpNodeInformation>> ListFilesAsync() 
            => _ftpClient.ListFilesAsync().ConfigureAwait(false);
        /// <inheritdoc cref="IFtpClient.ListDirectoriesAsync"/>
        public ConfiguredTaskAwaitable<ReadOnlyCollection<FtpNodeInformation>> ListDirectoriesAsync() 
            => _ftpClient.ListDirectoriesAsync().ConfigureAwait(false);
        /// <inheritdoc cref="IFtpClient.ChangeWorkingDirectoryAsync"/>
        public ConfiguredTaskAwaitable ChangeWorkingDirectoryAsync(string directory)
            => _ftpClient.ChangeWorkingDirectoryAsync(directory).ConfigureAwait(false);
        /// <inheritdoc cref="IFtpClient.DeleteFileAsync"/>
        public ConfiguredTaskAwaitable DeleteFileAsync(string fileName) => 
            _ftpClient.DeleteFileAsync(fileName).ConfigureAwait(false);
        /// <inheritdoc cref="IFtpClient.DeleteFileAsync"/>
        public ConfiguredTaskAwaitable RenameAsync(string from, string to) => 
            _ftpClient.RenameAsync(from, to).ConfigureAwait(false);
        /// <inheritdoc cref="IFtpClient.IsConnected"/>
        public bool IsConnected => _ftpClient.IsConnected;

        /// <summary>
        /// Create this directory if it doesn't exists yet
        /// </summary>
        /// <param name="dirName"></param>
        /// <returns></returns>
        public async Task EnsureDirAsync(string dirName)
        {
            var dirs = await _ftpClient.ListDirectoriesAsync().ConfigureAwait(false);
            if (!dirs.Select(dir => dir.Name).Contains(dirName))
                await _ftpClient.CreateDirectoryAsync(dirName).ConfigureAwait(false);
        }

        public async Task ApproveFileAsync(string fileName)
        {
            await EnsureDirAsync(ImportConstants.ProcessedFolderName).ConfigureAwait(false);
            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);
            await RenameAsync(fileName,
                Path.Combine(ImportConstants.ProcessedFolderName, $"[{DateTime.UtcNow.Ticks}] {fileName}"));
        }

        public async Task RejectFileAsync(string fileName)
        {
            await EnsureDirAsync(ImportConstants.FailedFolderName).ConfigureAwait(false);
            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);
            await RenameAsync(fileName,
                Path.Combine(ImportConstants.FailedFolderName, $"[{DateTime.UtcNow.Ticks}] {fileName}"));
        }

        public async Task<List<FtpNodeInformation>> ListFilesWithExtensionAsync(IEnumerable<string> extensions)
        {
            var allFiles = (await ListFilesAsync()).ToList();
            if (allFiles.Count == 0) return allFiles;
            if (extensions == null) return allFiles;

            return allFiles.Where(file =>
                    extensions.Contains(new FileInfo(file.Name).Extension.Trim('.')))
                .ToList();
        }
    }
}
