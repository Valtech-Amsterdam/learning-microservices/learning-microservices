﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;

namespace KeyVault
{
    /// <summary>
    /// Class for accessing KeyVault secrets
    /// </summary>
    public struct KeyVaultReader
    {
        private static readonly KeyVaultClient KeyVaultClient;

        static KeyVaultReader()
        {
            var azureServiceTokenProvider = new AzureServiceTokenProvider();
            var callback = new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback);

            KeyVaultClient = new KeyVaultClient(callback);
        }

        /// <summary>
        /// Get the azure KeyVault secret by Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static async Task<string> GetSecretAsync(string key)
        {
            if(string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));
            var secretkey = Environment.GetEnvironmentVariable(key);
            if (string.IsNullOrWhiteSpace(secretkey))
                throw new NullReferenceException($"{nameof(secretkey)} -> {key}");

            return (await KeyVaultClient
                .GetSecretAsync(secretkey)
                .ConfigureAwait(false))?.Value;
        }
    }
}
