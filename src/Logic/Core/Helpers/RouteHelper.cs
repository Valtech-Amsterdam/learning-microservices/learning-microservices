﻿using System;
using System.IO;
using System.Net.Http;

namespace Core.Helpers
{
    public struct RouteHelper
    {
        public static Uri GetRelativeRoute(HttpRequestMessage req, string path, string queryOverride = null)
        {
           var baseUrl = req.RequestUri;

            if (queryOverride != null)
                return new UriBuilder(baseUrl) { Path = path, Query = queryOverride }.Uri;
            return new UriBuilder(baseUrl) { Path = path }.Uri;
        }

        public static Uri GetAbsolutePath(string functionCategory, string path, string queryOverride = null)
        {
            if (string.IsNullOrWhiteSpace(functionCategory)) throw new ArgumentNullException(nameof(functionCategory));
            var baseUrlString = Environment.GetEnvironmentVariable($"Url-{functionCategory}");
            if (string.IsNullOrWhiteSpace(baseUrlString)) throw new NullReferenceException($"Url-{functionCategory}");

            var baseUrl = new UriBuilder(baseUrlString);
            if (queryOverride != null) baseUrl.Query = queryOverride;
            baseUrl.Path = Path.Combine(baseUrl.Path, path.TrimStart('/'));

            return baseUrl.Uri;
        }
    }
}
