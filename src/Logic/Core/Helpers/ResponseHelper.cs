﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Formatting = Core.Constants.Formatting;

namespace Core.Helpers
{
    public struct ResponseHelper
    {
        public static HttpResponseMessage CreateObjectResponse<TResponseClass>(TResponseClass responseObject)
            => new HttpResponseMessage
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(responseObject, Formatting.JsonSerializerSettings), 
                    Encoding.UTF8, "application/json")
            };
    }
}
