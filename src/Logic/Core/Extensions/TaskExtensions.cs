﻿using System.Threading.Tasks;

namespace Core.Extensions
{
    /// <summary>
    /// Sytstem tasks extensions
    /// </summary>
    public static class TaskExtensions
    {
        /// <summary>
        /// Run a task in a synchronous fashion for constructors or dependency injection
        /// please try to use async where possible
        /// </summary>
        /// <typeparam name="TAwaited"></typeparam>
        /// <param name="task"></param>
        /// <returns></returns>
        public static TAwaited GetResultSynchronously<TAwaited>(this Task<TAwaited> task)
            => task
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();

        /// <summary>
        /// Run a task in a synchronous fashion for constructors or dependency injection
        /// please try to use async where possible
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public static void ExcecuteSynchronously(this Task task)
            => task
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();
    }
}