﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Extensions
{
    /// <summary>
    /// Making linq prettier
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Flatten multiple Enumerables one level down
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IEnumerable<TModel> Flatten<TModel>(this IEnumerable<IEnumerable<TModel>> collection) => collection.SelectMany(inner => inner);

        /// <summary>
        /// Flatten multiple enumerables one level down and apply a selector
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="collection"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> FlatSelect<TSource, TResult>(this IEnumerable<IEnumerable<TSource>> collection, Func<TSource, TResult> selector)
            => collection.Flatten().Select(selector);

        /// <inheritdoc cref="FlatSelect{TSource,TResult}(IEnumerable{System.Collections.Generic.IEnumerable{TSource}},Func{TSource,TResult})"/>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="collection"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> FlatSelect<TSource, TResult>(this IEnumerable<IEnumerable<TSource>> collection, Func<TSource, int, TResult> selector)
            => collection.Flatten().Select(selector);
    }
}
