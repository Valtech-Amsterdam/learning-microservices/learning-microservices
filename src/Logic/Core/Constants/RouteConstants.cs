﻿namespace Core.Constants
{
    public struct RouteConstants
    {
        public struct Asset
        {
            public const string GetAssetPath = "asset/{*assetPath}";
            public const string GetAssetReferencePath = "assetReference/{productId}";
            public const string GetImagePath = "image/{*imagePath}";
        }

        public struct Availability
        {
            public const string GetAvailabilityPath = "availabilty/{productId}";
            public const string IsAvailablePath = "available/{productId}";
        }

        public struct Price
        {
            public const string GetCurrentPricePath = "price/current/{productId}";
            public const string HasValidPricePath = "price/valid/{productId}";
            public const string ListPricesPath = "price/all/{productId}";
        }

        public struct Product
        {
            public const string GetProductPath = "product/{productId}";
            public const string ListAvailableProductsPath = "availableProducts";
            public const string ListProductsPath = "product";
        }
    }
}
