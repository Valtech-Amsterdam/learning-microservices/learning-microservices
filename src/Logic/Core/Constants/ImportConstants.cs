﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Core.Constants
{
    public struct ImportConstants
    {
        public const string DataImportFileExtension = "yml";
        public const string ProcessedFolderName = "Processed";
        public const string FailedFolderName = "Failed";
    }

    public struct Formatting
    {
        public static JsonSerializerSettings JsonSerializerSettings => new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Formatting = Newtonsoft.Json.Formatting.None,
            DateFormatString = "yyyy-MM-ddTHH:mmZ",
            NullValueHandling = NullValueHandling.Ignore
        };
    }
}
