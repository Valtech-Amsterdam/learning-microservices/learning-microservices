﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Models;
using FluentValidation;
using Ftp;
using Ftp.Models;
using ImportData.Extensions;
using Reporting.Client;

namespace ImportData.Models
{
    public struct DataValidator<TModel>
    {
        public static async Task<IEnumerable<FileResult<UpdateCommand<TModel>>>> ValidateFiles(
            IEnumerable<FileResult<UpdateCommand<TModel>>> parsedFiles, FtpConnection ftpConnection, 
            IValidator<UpdateCommand<TModel>> modelValidator, ReportClient reportClient)
        {
            if (modelValidator == null) return parsedFiles;

            var validationResult = await Task.WhenAll(parsedFiles.Select(async file =>
            {
                if (file == null) return null;
                try
                {
                    if (file.Data == null)
                    {
                        await reportClient.ReportDataError($"{file.Name}: Empty files are not allowed!");
                        await ftpConnection.RejectFileAsync(file.Name);
                        return null;
                    }

                    var importValidation = modelValidator.Validate(file.Data);
                    if (importValidation.IsValid) return file;

                    await reportClient.ReportValidationError($"{file.Name}: {importValidation.ToReportLine()}");
                    await ftpConnection.RejectFileAsync(file.Name);
                    return null;
                }
                catch (Exception ex)
                {
                    await reportClient.ReportException($"{file.Name}: { ex }");
                    await ftpConnection.RejectFileAsync(file.Name);
                    return null;
                }
            }))
            .ConfigureAwait(false);

            return validationResult.Where(x => x != null);
        }
    }
}
