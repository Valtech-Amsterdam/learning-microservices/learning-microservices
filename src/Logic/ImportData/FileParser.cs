﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ftp;
using Ftp.Models;
using Reporting.Client;
using YamlDotNet.Core;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ImportData
{
    public struct FileParser<TModel>
    {
        public static async Task<IEnumerable<FileResult<TModel>>> ParseFiles(List<string> fileNames,
            FtpConnection ftpConnection, ReportClient reportClient)
        {
            var fileResults = await Task.WhenAll(fileNames.Select(async fileName =>
                {
                    using (var fileStream = ftpConnection.OpenFileReadStream(fileName))
                    using (var streamReader = new StreamReader(fileStream))
                    {
                        return await ParseFileStream(
                            fileName, streamReader, ftpConnection, reportClient).ConfigureAwait(false);
                    }
                }))
                .ConfigureAwait(false);

            return fileResults
                .Where(fileResult => fileResult != null);
        }

        private static async Task<FileResult<TModel>> ParseFileStream(string fileName, StreamReader streamReader,
            FtpConnection ftpConnection, ReportClient reportClient)
        {
            try
            {
                var deserializer = new DeserializerBuilder()
                    .WithNamingConvention(new CamelCaseNamingConvention())
                    .WithTypeConverter(new CustomDateTimeConverter())
                    .Build();

                return new FileResult<TModel>
                {
                    Name = fileName,
                    Data = deserializer.Deserialize<TModel>(streamReader)
                };
            }
            catch (YamlException ex) when (!(ex.InnerException is null))
            {
                await reportClient.ReportDataError($"{fileName}: \n{ ex.Start}: { ex.InnerException.Message }");
                await ftpConnection.RejectFileAsync(fileName).ConfigureAwait(false);
                return null;
            }
            catch (Exception ex)
            {
                await reportClient.ReportException($"{fileName}: \n{ ex }: { ex.Message}");
                return null;
            }
        }
    }
}
