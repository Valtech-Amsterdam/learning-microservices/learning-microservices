﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;

namespace ImportData.Extensions
{
    public static class ValidationDataExtensions
    {
        public static string ToReportLine(this ValidationException exception)
            => exception.Errors.ToReportLine();

        public static string ToReportLine(this ValidationResult validationResult)
            => validationResult.Errors.ToReportLine();

        public static string ToReportLine(this IEnumerable<ValidationFailure> failures)
        {
            return string.Join("\n",
                failures.Select(error => $" - {error.PropertyName}: {error.ErrorMessage}"));
        }
    }
}
