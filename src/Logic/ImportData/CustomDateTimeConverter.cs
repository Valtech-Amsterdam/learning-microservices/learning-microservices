﻿using System;
using System.Globalization;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace ImportData
{
    public class CustomDateTimeConverter : IYamlTypeConverter
    {
        protected static readonly CultureInfo CultureInfo = new CultureInfo(CultureInfo.InvariantCulture.Name)
        {
            DateTimeFormat =
            {
                ShortDatePattern = "ddMMyyyy",
                LongDatePattern = "ddMMyyyy",
                ShortTimePattern = "HHmm",
                LongTimePattern = "HHmm",
                FullDateTimePattern = "ddMMyyyy HHmm"
            }
        };

        /// <summary>
        /// Gets a value indicating whether the current converter supports converting the specified type.
        /// </summary>
        /// <param name="type"><see cref="T:System.Type" /> to check.</param>
        /// <returns>Returns <c>True</c>, if the current converter supports; otherwise returns <c>False</c>.</returns>
        public bool Accepts(Type type) => type == typeof(DateTime) || type == typeof(DateTime?);

        /// <summary>Reads an object's state from a YAML parser.</summary>
        /// <param name="parser"><see cref="T:YamlDotNet.Core.IParser" /> instance.</param>
        /// <param name="type"><see cref="T:System.Type" /> to convert.</param>
        /// <returns>Returns the <see cref="T:System.DateTime" /> instance converted.</returns>
        /// <remarks>On deserializing, all formats in the list are used for conversion.</remarks>
        public object ReadYaml(IParser parser, Type type)
        {
            var dateTimeString = ((Scalar)parser.Current).Value;
            if (type == typeof(DateTime?) && string.IsNullOrWhiteSpace(dateTimeString)) return null;

            var dateTime = DateTime.ParseExact(
                dateTimeString, CultureInfo.DateTimeFormat.FullDateTimePattern, 
                CultureInfo, DateTimeStyles.AssumeUniversal);

            parser.MoveNext();
            return dateTime.ToUniversalTime();
        }

        /// <summary>
        /// Writes the specified object's state to a YAML emitter.
        /// </summary>
        /// <param name="emitter"><see cref="T:YamlDotNet.Core.IEmitter" /> instance.</param>
        /// <param name="value">Value to write.</param>
        /// <param name="type"><see cref="T:System.Type" /> to convert.</param>
        /// <remarks>On serializing, the first format in the list is used.</remarks>
        public void WriteYaml(IEmitter emitter, object value, Type type)
        {
            var dateTime = (DateTime?)value;
            var str = dateTime?.ToUniversalTime().ToString(CultureInfo);
            emitter.Emit(new Scalar(null, null, str, ScalarStyle.Any, true, false));
        }
    }
}
