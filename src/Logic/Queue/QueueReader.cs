﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Extensions;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace Queue
{
    /// <summary>
    /// This class provides functionality to get all the items from an Azure Queue
    /// </summary>
    public struct QueueReader
    {
        private const int QueueStep = 32;
        private static readonly TimeSpan VisibilityTimeout = // ReSharper disable once AssignNullToNotNullAttribute
            TimeSpan.FromMinutes(double.Parse(Environment.GetEnvironmentVariable("Queue-VisibilityTimeout")));
        private static readonly QueueRequestOptions QueueRequestOptions = new QueueRequestOptions();
        private static readonly OperationContext OperationContext = new OperationContext();

        /// <summary>
        /// Loop through the <paramref name="reportQueue"/> and get all the items that are currently queued
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="reportQueue"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<CloudQueuedItem<TItem>>> ReadCloudQueue<TItem>(CloudQueue reportQueue)
        {
            // Todo C# 8 async stream
            reportQueue.FetchAttributes();
            return (await Task.WhenAll(ReadQueuedReportLines(reportQueue)))
                .FlatSelect(ConvertToItemModel<TItem>);
        }

        private static CloudQueuedItem<TItem> ConvertToItemModel<TItem>(CloudQueueMessage message)
            => new CloudQueuedItem<TItem>
                {
                    Id = message.Id,
                    PopReceipt = message.PopReceipt,
                    Item = JsonConvert.DeserializeObject<TItem>(message.AsString)
                };

        private static IEnumerable<Task<IEnumerable<CloudQueueMessage>>> ReadQueuedReportLines(CloudQueue reportQueue)
        {
            var size = reportQueue.ApproximateMessageCount;
            for (var i = 0; i < size; i += QueueStep)
                yield return GetNextReportLines(reportQueue);
        }

        private static async Task<IEnumerable<CloudQueueMessage>> GetNextReportLines(CloudQueue reportQueue)
            => await reportQueue
                .GetMessagesAsync(QueueStep, VisibilityTimeout, QueueRequestOptions, OperationContext);
    }
}
