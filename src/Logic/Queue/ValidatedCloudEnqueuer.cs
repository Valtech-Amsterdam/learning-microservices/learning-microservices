﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace Queue
{
    /// <summary>
    /// Class that validates a model and pushes it to the CloudQueue if valid
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public sealed class ValidatedCloudEnqueuer<TModel>
        where TModel : class, new()
    {
        private readonly TraceWriter _log;
        private readonly CloudQueue _reportQueue;
        private readonly IValidator<TModel> _modelValidator;

        public ValidatedCloudEnqueuer(TraceWriter log, CloudQueue reportQueue, IValidator<TModel> modelValidator)
        {
            _log = log;
            _reportQueue = reportQueue;
            _modelValidator = modelValidator;
        }


        /// <summary>
        /// Validate <paramref name="report"/> and Enqueue into the CloudQueue
        /// </summary>
        /// <param name="report"></param>
        /// <returns>Httpstatus of OK when valid otherwise NotAcceptable with a summary of errors</returns>
        public async Task<HttpResponseMessage> Enqueue(TModel report)
        {
            _log.Info("Enqueueing new Report");

            if (_modelValidator is null)
                return await QueueAndReturn(report);

            var validationResults = _modelValidator.Validate(report);
            if (!validationResults.IsValid) return new HttpResponseMessage(HttpStatusCode.NotAcceptable)
            {
                Content = new StringContent(string.Join("\n", validationResults.Errors.Select(error => error.ErrorMessage)))
            };

            return await QueueAndReturn(report);
        }

        private async Task<HttpResponseMessage> QueueAndReturn(TModel report)
        {
            await _reportQueue.AddMessageAsync(new CloudQueueMessage(JsonConvert.SerializeObject(report)));
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
