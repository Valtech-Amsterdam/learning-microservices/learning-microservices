﻿using Microsoft.WindowsAzure.Storage.Queue.Protocol;

namespace Queue
{
    /// <summary>
    /// Wrapper for carying deserialized Azure Queue items while holding a reference to delete them from the Queue at a later point
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public sealed class CloudQueuedItem<TItem>
    {
        /// <inheritdoc cref="QueueMessage.Id"/>
        public string Id { get; set; }

        /// <inheritdoc cref="QueueMessage.PopReceipt"/>
        public string PopReceipt { get; set; }

        /// <summary>
        /// The deserialized Azure Queue Message
        /// </summary>
        public TItem Item { get; set; }
    }
}
