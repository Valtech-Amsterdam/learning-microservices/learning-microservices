import React, { Component } from "react";

class Label extends Component {
    
    render() {

        if (this.props.value == null) 
            return <span className={this.props.className + ' no-data'}>No {this.props.name} available</span>

        return <span className={this.props.className}>
            {this.props.value}
        </span>
    }
}

export default Label;