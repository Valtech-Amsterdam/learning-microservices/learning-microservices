import React, { Component } from "react";
import "./FramedImage.css"


class FramedImage extends Component {

    constructor() {
        super()
        this.frameChanged = this.frameChanged.bind(this)
        this.buildFrameSelector = this.buildFrameSelector.bind(this)
    }

    getFrameName(frame) {
        switch (frame) {
            default: return 'No frame';
            case 1: return 'Classic';
            case 2: return 'Modern slick';
            case 3: return 'Modern thick';
        }
    }

    frameChanged(event) {
        const value = event.target.value;
        this.props.onChange(value)
    }

    buildFrameSelector() {

        const { frames, selectedFrame } = this.props;
        const getFrameName = this.getFrameName;

        if(frames.length === 1) return <span>{getFrameName(selectedFrame)}</span>

        return <select id="frameSelector" value={selectedFrame} onChange={this.frameChanged}>
            {frames.map(frame => {
                return <option key={frame} value={frame} >{getFrameName(frame)}</option>
            })}
        </select>
    }

    render() {

        return <div className="frameSelector">
            <label htmlFor="frameSelector">Type of frame</label>
            {this.buildFrameSelector()}
        </div>
    }
}

export default FramedImage;