import React, { Component } from "react";
import Spinner from "./Spinner";
import Label from "./Label";

class PriceLabel extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loaded: false,
            value: null
        }

        this.formatPrice = this.formatPrice.bind(this)
    }

    componentDidMount() {
        this.props.pricePromise.then(price => {
            this.setState({
                loaded: true,
                value: price.data
            })
        })
    }

    formatPrice() {
        const cents = this.state.value;
        if(cents == null) return null
    
        const price = cents / 100;
        
        if (price % 1 === 0)
            return `☄ ${price}.–`
    
        return `☄ ${price.toFixed(2)}`
    }

    render(){
        const { loaded, value } = this.state;

        if (!loaded) 
            return <span className="price loading"><Spinner className='small' /></span>

        return <Label name="price" className="price loaded" value={this.formatPrice(value)} />
    }
}

export default PriceLabel;