import React, { Component } from "react";
import Spinner from "./Spinner";

class AvailabilityLabel extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loaded: false,
            value: null
        }
    }

    componentDidMount() {
        this.props.availabilityPromise.then(availability => {
            this.setState({
                loaded: true,
                value: availability
            })
        })
    }

    render(){
        const { loaded, value } = this.state;

        if (!loaded) 
            return <span className="availability loading"><Spinner className='small' /></span>
        
            
        if (value === null) 
            return <span className='availability no-data'>No availability available</span>
            
        if (value) return null;
        
        return <span className='availability unavailable'>
            This product is currently unavailable!
        </span>
    }
}

export default AvailabilityLabel;