import React, { Component } from "react";
import Label from "./Label";

class CategoryLabel extends Component {

    constructor(props) {
        super(props)

        this.formatCategory = this.formatCategory.bind(this)
    }

    formatCategory() {
        const category = this.props.category;
        if(category == null) return null
    
        switch (category) {
            default: return null;
            case 0: return 'Oil-painting';
            case 1: return 'Water colour';
            case 2: return 'Acrylic';
            case 3: return 'Pastel';
            case 4: return 'Glass';
            case 5: return 'Fresco';
            case 6: return 'Encaustic';
            case 7: return 'Gouache';
            case 8: return 'Spray painting';
            case 9: return 'Tempera';
            case 10: return 'Digital';
            case 11: return 'Sand';
            case 12: return 'Collage';
            case 13: return 'Kalamkari';
        }
    }

    render = () => <Label name="category" className="category" value={this.formatCategory()} />
}

export default CategoryLabel;