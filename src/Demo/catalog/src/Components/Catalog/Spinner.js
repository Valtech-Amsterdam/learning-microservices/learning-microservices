import React, { Component } from "react";
import './Spinner.css';

class Spinner extends Component {
    
    render = () => <span className={`spinner ${this.props.className}`}>
        <span className="lds-ring"><span></span><span></span><span></span><span></span></span></span>
}

export default Spinner;