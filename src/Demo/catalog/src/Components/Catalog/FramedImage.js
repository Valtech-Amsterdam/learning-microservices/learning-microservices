import React, { Component } from "react";
import "./FramedImage.css"
import "./ClassicFrame.css"
import ProductImage from "./ProductImage";
import Spinner from "./Spinner";

const maxSize = 350;

class FramedImage extends Component {

    constructor(props){
        super(props)

        this.state = {
            loaded: false,
            value: null,
            width: maxSize,
            height: maxSize
        }

        this.calculateDimensions = this.calculateDimensions.bind(this)
    }

    roundDigit = (digit) => Math.floor(digit * 100) / 100;

    calculateDimensions(aspectRatio, maxSize) {
        if(!!!aspectRatio) return { width: maxSize, height: maxSize }

        const widthRatioQualifier = aspectRatio.split(':')[0];
        const heigtRatioQualifier = aspectRatio.split(':')[1];
        const isLandscape = widthRatioQualifier >= heigtRatioQualifier;

        if (isLandscape) 
            return {
                width: maxSize,
                height: this.roundDigit(maxSize * (heigtRatioQualifier / widthRatioQualifier))
            };
            
        return {
            width: this.roundDigit(maxSize * (widthRatioQualifier / heigtRatioQualifier)),
            height: maxSize
        };
    }

    componentDidMount() {
        this.props.assetReferencePromise.then(assetReference => {

            const aspectRatio = assetReference.data === null ?  null : assetReference.data.aspectRatio;
            const { width, height } = this.calculateDimensions(aspectRatio, maxSize)

            this.setState({
                loaded: true,
                width,
                height
            })
        })
    }

    render() {

        const { selectedFrame } = this.props;
        const { width , height, loaded } = this.state;
        const assetReferencePromise = this.props.assetReferencePromise

        if(!loaded) return <div className="framedImage" style={{ width, height }}>
            <div className="lazyImage" style={{ width, height }}>
                <Spinner />
            </div>
        </div>

        return <div className="framedImage" style={{ width, height }}>
            <div className={ "frame frame-" + selectedFrame}>
                <div className="lt"></div>  <div className="t"></div>   <div className="rt"></div>
                <div className="l"></div>                               <div className="r"></div>
                <div className="lb"></div>  <div className="b"></div>   <div className="rb"></div>
            </div>
            <ProductImage assetReferencePromise={assetReferencePromise} width={width} height={height} />
        </div>
    }
}

export default FramedImage;