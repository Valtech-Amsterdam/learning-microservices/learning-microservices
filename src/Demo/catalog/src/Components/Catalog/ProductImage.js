import { preloadApiImage } from '../../Util/Api/ImageApi'
import LazyImage from "../Site/LazyImage";

class ProductImage extends LazyImage {

    constructor(props) {
        super(props)

        this.loadImage = this.loadImage.bind(this)
        this.loadProductImage = this.loadProductImage.bind(this)
    }

    loadImage() {
        this.loadProductImage()
            .then(this.setImageState)
    }

    async loadProductImage(){

        const { assetReferencePromise, width, height, mode } = this.props;

        const assetReference = await assetReferencePromise;
        const assetUrl = assetReference.responseCode !== 200 ? null : assetReference.data.url

        return await preloadApiImage(assetUrl, width, height, mode)
    }
}

export default ProductImage;