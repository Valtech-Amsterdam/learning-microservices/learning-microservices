import React, { Component } from "react";
import Label from './Label';
import PriceLabel from './PriceLabel';
import CategoryLabel from './CategoryLabel';
import AvailabilityLabel from './AvailabilityLabel';
import FramedImage from "./FramedImage";
import FrameSelector from "./FrameSelector";

class ProductDetail extends Component {

    constructor(data) {
        super()
        this.state = {
          selectedFrame: data.product.frames[0]
        }

        this.frameChanged = this.frameChanged.bind(this)
    }

    frameChanged(value) {
      this.setState({
        selectedFrame: value
      })
    }
    
    render() {
      const { selectedFrame } = this.state;
      const { id, artist, name, frames, type, description } = this.props.product;

      return <div className='product' data-id={id}>

        <h2>{name}</h2>

        <div className='banner'>

          <FramedImage assetReferencePromise={this.props.assetReferencePromise} selectedFrame={selectedFrame} />
          
          <div className="productData">
            <div className='simpleData'>
              <p>
                <Label name="artist" value={artist} /><br />
                <CategoryLabel category={type} /><br />
                <AvailabilityLabel availabilityPromise={this.props.availabilityPromise} />                
              </p>
              <p>
                <PriceLabel pricePromise={this.props.pricePromise} />
              </p>
            </div>
            <FrameSelector frames={frames} selectedFrame={selectedFrame} onChange={(value) => this.frameChanged(value)} />
          </div>
        </div> 
        <div className="afterBanner"></div>

        <article dangerouslySetInnerHTML={{__html: description}} />
      </div>
    }
}

export default ProductDetail;