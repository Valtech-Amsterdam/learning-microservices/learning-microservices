import React, { Component } from "react";
import { Link } from "react-router-dom";
import Label from './Label';
import PriceLabel from './PriceLabel';
import ProductImage from './ProductImage';

class ProductListItem extends Component {

  constructor(props){
    super(props)

    this.state = {
      price: null,
      assetReference: null
    };

    this.handlePriceUpdate = this.handlePriceUpdate.bind(this)
  }

  async handlePriceUpdate() {
    const price = await this.props.pricePromise;
    this.setState({ 
      price: price
    })
  }

  componentDidMount() {

    this.handlePriceUpdate()
  }
    
  render() {
  
    const { id, artist, name } = this.props.product;

    return <Link to={{ pathname: `/catalog/product/${id}`, state: this.props }} data-id={id}>

      <ProductImage assetReferencePromise={this.props.assetReferencePromise} width={250} height={150} mode="crop" />
      <PriceLabel pricePromise={this.props.pricePromise} />
      <Label name="exordium" className="exordium" value={ `${name} by ${artist}` } />
    </Link>
  }
}

export default ProductListItem;