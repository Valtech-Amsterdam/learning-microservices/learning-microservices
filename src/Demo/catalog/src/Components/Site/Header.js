import React, { Component } from "react";
import { Link } from 'react-router-dom'
import './Header.css'
import { preloadApiImage } from '../../Util/Api/ImageApi'

const headerBarSize = 80;

class Header extends Component {

  constructor(){
    super()

    const documentHeight = document.body.getBoundingClientRect().height;
    const documentWidth = document.body.getBoundingClientRect().width;
    const scrollTop = document.body.getBoundingClientRect().top;

    this.state = {
      loaded: false,
      documentHeight,
      documentWidth,
      scrollTop,
      backgroundImageUrl: null
    };

    this.onScroll = this.onScroll.bind(this)
    this.loadImage = this.loadImage.bind(this)
    this.renderImage = this.renderImage.bind(this)
}

  loadImage(){
      preloadApiImage(
        'flower-girl-in-holland.jpg', 
        window.screen.availWidth, 
        window.screen.availHeight, 
        'crop')
      .then(data => this.renderImage(data))
  }

  renderImage(imageData) {

    if(imageData.responseCode !== 200) return this.setState({ 
        loaded: true,
        backgroundImageUrl: null 
    }); 
    
    this.setState({ 
        loaded: true,
        backgroundImageUrl: imageData.data
    });
  }

  componentDidMount() {
    this.appElement = document.getElementsByClassName("App")[0];
    this.appElement.addEventListener('scroll', this.onScroll.bind(this))
    
    this.loadImage()
  }

  onScroll(){
    const isHomePage = window.location.hash === "#/";
    if(!isHomePage) return;

    const scrollTop = this.appElement.firstChild.getBoundingClientRect().top;

    this.setState({
      scrollTop
    });
  }

  render() {
    let elementHeight = this.state.scrollTop + this.state.documentHeight;
    if(elementHeight <= headerBarSize) elementHeight = headerBarSize;
    const offsetTop = -this.state.scrollTop;
    const backgroundImage = `url(${this.state.backgroundImageUrl})`;

    const headerSizing = {
      height: elementHeight,
      top: offsetTop,
      backgroundImage
    };

    const navPos = {
      top: elementHeight > headerBarSize ? offsetTop : elementHeight
    }
    
    const isHomePage = window.location.hash === "#/";
    if(isHomePage) 
      return <header className='homeHeader'>
        <div className='headerImage' style={headerSizing}>
          <div id="header">
            <h1 height={elementHeight}>Example shop</h1>
          </div>
        </div>
        <Navigation style={navPos} />
      </header>;

    return <header className='headerImage' style={{ backgroundImage }}>
      <a href="/" id="header">
        <h1>Example shop</h1>
      </a>
      <Navigation />
    </header>;
  }
}

class Navigation extends Component {
  render() {
    return <nav style={this.props.style}>
      <ul>
        <li><Link to={{ pathname: '/'}}>Home</Link></li>
        <li><Link to={{ pathname: '/architecture'}}>Architecture</Link></li>
        <li><Link to={{ pathname: '/catalog'}}>Catalog</Link></li>
      </ul>
    </nav>;
  }
}

export default Header;
