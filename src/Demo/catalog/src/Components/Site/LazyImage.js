import React, { Component } from "react";
import { preloadImage } from '../../Util/Api/ImageApi'
import Spinner from "../Catalog/Spinner";
import "./LazyImage.css";

class LazyImage extends Component {

  constructor(props){
    super(props)

    this.state = {
      loaded: false,
      imageUrl: null
    };

    this.loadImage = this.loadImage.bind(this)
    this.setImageState = this.setImageState.bind(this)
    this.renderImage = this.renderImage.bind(this)

    this.rootRef = React.createRef();
  }

  loadImage() {
    const { url } = this.props;
    if(url == null) return this.setImageState({ success: false });
    preloadImage(url).then(this.setImageState)
  }

  setImageState(imageData) {
    
      if(imageData.responseCode !== 200) return this.setState({ 
          loaded: true,
          imageUrl: null 
      }); 
      
      this.setState({ 
          loaded: true,
          imageUrl: imageData.data
      });
  }

  componentDidMount() {        
    this.observer = new IntersectionObserver((intersector) => {
      if(this.state.loaded) return;
      if(!intersector[0].isIntersecting) return;
      this.loadImage();
    }, {
      root: document.body,
      rootMargin: `0px 0px ${this.props.height || 0}px 0px`,
      threshold: 0
    });
    this.observer.observe(this.rootRef.current)
  }

  render() {
    const { width, height } = this.props;

    const className = this.props.className === null || this.props.className === undefined || this.props.className === String() ? 
      "lazyImage" : "lazyImage " + this.props.className
    return <span className={className} style={{ width, height }} ref={this.rootRef}>
        {this.renderImage()}
    </span>
  }

  renderImage() {
    const { url, width, height } = this.props;
    const { imageUrl, loaded } = this.state;

    if(!loaded) return <Spinner />

    if(url === null || imageUrl === null) return <p className="noImage ">No image available</p>

    return <img src={imageUrl} width={width} height={height} alt={this.props.alt}/>
  }
}

export default LazyImage;