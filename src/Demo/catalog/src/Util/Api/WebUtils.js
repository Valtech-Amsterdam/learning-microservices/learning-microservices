export class ApiResponse {

    constructor(responseCode, data){
        this.responseCode = responseCode
        this.data = data
    }
}

export async function tryRequest(requestFunction){
    try{
        return await requestFunction().catch(e => { throw e; });
    } catch(err){
        if (err.message === 'Failed to fetch')
            return new ApiResponse(408, null)

        console.error(err);
        return new ApiResponse(500, null)
    }
}

export function get(url) { 
    return tryRequest(async () => {
        const response = await fetch(url)
        if(!response.ok) return new ApiResponse(response.status, null)

        return new ApiResponse(response.status, await response.json());
    })
}

export function post(url, body) { 
    return tryRequest(async () => {
        const response = await fetch(url, {
            body,
            headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            }
        })
        if(!response.ok) return new ApiResponse(response.status, null)
        return  new ApiResponse(response.status, await response.json())
    })
}

const toString = (obj) => !!!obj ? null : obj.toString()
export function combineUrl(baseUrl, ...urlParts) {
    const cleanBaseUrl = baseUrl.trim().replace(/(\/)+$/gi, String())
    const path = Array.from(urlParts)
        .map(toString)
        .filter(x => x != null)
        .join('/')

    return `${cleanBaseUrl}/${path}`;
}