import { combineUrl, get, ApiResponse } from './WebUtils';
import { isProductApiEnabled, isAvailabilityApiEnabled } from '../Debug/ConsoleHelper';
import Constants from '../../Constants'

const getUrl = (...urlParts) => combineUrl(Constants.apiEndpoints.products, ...urlParts)

export class ProductData {

    constructor(
        id, type, frames, artist, description
    ){
        this.id = id;

        this.type = type;
        this.frames = frames | [];
        this.artist = artist;
        this.description = description;
    }
}

export async function getProduct(id) {
    if(!isProductApiEnabled()) return new ApiResponse(503, null);

    const url = getUrl('product', id)

    return await get(url)
}

export async function getProducts(skip = 0, take = 10) {
    if(!isProductApiEnabled()) return new ApiResponse(503, null);
    if(!isAvailabilityApiEnabled()) return new ApiResponse(200, []);

    const url = `${getUrl('availableProducts')}?skip=${skip}&take=${take}`

    return await get(url)
}