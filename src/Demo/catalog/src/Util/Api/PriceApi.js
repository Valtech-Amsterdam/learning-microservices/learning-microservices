import { combineUrl, get, ApiResponse } from './WebUtils';
import { isPriceApiEnabled } from '../Debug/ConsoleHelper';
import Constants from '../../Constants'

const getUrl = (...urlParts) => combineUrl(Constants.apiEndpoints.prices, ...urlParts)

export async function getCurrentPrice(id) {
    if(!isPriceApiEnabled()) return new ApiResponse(503, null);

    const url = getUrl('price', 'current', id)

    return await get(url)
}