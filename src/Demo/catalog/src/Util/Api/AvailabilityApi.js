import { combineUrl, get, ApiResponse } from './WebUtils';
import { isAvailabilityApiEnabled } from '../Debug/ConsoleHelper';
import Constants from '../../Constants'

const getUrl = (...urlParts) => combineUrl(Constants.apiEndpoints.availability, ...urlParts)

export async function isAvailable(id) {
    if(!isAvailabilityApiEnabled()) return new ApiResponse(503, null);

    const url = getUrl('available', id)

    return await get(url)
}