import { combineUrl, get, ApiResponse, tryRequest } from './WebUtils';
import { isAssetApiEnabled } from '../Debug/ConsoleHelper';
import Constants from '../../Constants'

const getUrl = (...urlParts) => combineUrl(Constants.apiEndpoints.assets, ...urlParts)

export async function preloadApiImage(path, width, height, mode) {
    if (path === null) return new ApiResponse(404, null);
    if(!isAssetApiEnabled()) return new ApiResponse(503, null);

    const modeParam = !!mode ? `&mode=${mode}` : String();
    const url = `${getUrl('image', path)}?h=${height}&w=${width}${modeParam}`

    return await preloadImage(url);
}

export async function preloadImage(url) {
    if (url === null) return new ApiResponse(404, null)

    return await tryRequest(async () => {
        const response = await fetch(url)
        return new ApiResponse(response.status, url)
    })
}

export async function getAssetReference(id) {
    if(!isAssetApiEnabled()) return new ApiResponse(503, null);

    const url = getUrl('assetReference', id)

    return await get(url);
}