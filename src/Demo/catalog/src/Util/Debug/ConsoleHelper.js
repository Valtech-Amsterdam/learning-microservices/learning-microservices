import Cookies from 'universal-cookie';
const cookies = new Cookies();

window.disablePriceApi = () => {
    cookies.set('priceApiEnabled', false, { path: '/' });
    window.location.reload()
}
window.enablePriceApi = () => {
    cookies.set('priceApiEnabled', true, { path: '/' });
    window.location.reload()
}
export function isPriceApiEnabled(){
    return cookies.get('priceApiEnabled') !== 'false';
}

window.disableProductApi = () => {
    cookies.set('productApiEnabled', false, { path: '/' });
    window.location.reload()
}
window.enableProductApi = () => {
    cookies.set('productApiEnabled', true, { path: '/' });
    window.location.reload()
}
export function isProductApiEnabled(){
    return cookies.get('productApiEnabled') !== 'false';
}

window.disableAssetApi = () => {
    cookies.set('assetApiEnabled', false, { path: '/' });
    window.location.reload()
}
window.enableAssetApi = () => {
    cookies.set('assetApiEnabled', true, { path: '/' });
    window.location.reload()
}
export function isAssetApiEnabled(){
    return cookies.get('assetApiEnabled') !== 'false';
}

window.disableAvailabilityApi = () => {
    cookies.set('availabilityApiEnabled', false, { path: '/' });
    window.location.reload()
}
window.enableAvailabilityApi = () => {
    cookies.set('availabilityApiEnabled', true, { path: '/' });
    window.location.reload()
}
export function isAvailabilityApiEnabled(){
    return cookies.get('availabilityApiEnabled') !== 'false';
}