import { getProduct, getProducts } from "../Api/ProductApi";
import { getCurrentPrice } from "../Api/PriceApi";
import { isAvailable } from "../Api/AvailabilityApi";
import { getAssetReference } from "../Api/ImageApi";
import { ApiResponse } from "../Api/WebUtils";

export class ProductModel{

    constructor(product, pricePromise, assetReferencePromise){
        this.product = product
        this.pricePromise = pricePromise
        this.assetReferencePromise = assetReferencePromise
    }

    availabilityPromise = Promise.resolve(true)
}

export async function getProductById(id) {
    const productResponse = await getProduct(id)
    if (productResponse.responseCode !== 200) return productResponse;

    let productModel = await buildProductModel(productResponse.data)
    productModel.availabilityPromise = isAvailable(productResponse.data.id).then(availability =>{
        
        if(availability.responseCode === 404)
            return false;
        if(availability.responseCode === 200)
            return availability.data || false;
            
        return null;
    })

    return new ApiResponse(200, productModel)
}

export async function listProducts(skip = 0, take = 10) {

    const productList = await getProducts(skip, take)
    if (productList.responseCode !== 200) return productList;

    return new ApiResponse(200,
        productList.data
            .map(product => {
                product.available = true;
                return product;
            })
            .map(buildProductModel))
}

function buildProductModel(productData){
    return new ProductModel(productData,
        getCurrentPrice(productData.id),
        getAssetReference(productData.id)
    )
}