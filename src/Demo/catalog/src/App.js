import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import './App.css';

import './Util/Debug/ConsoleHelper'

import Header from './Components/Site/Header';

import Home from './Pages/Home';
import Catalog from './Pages/Catalog';
import Product from './Pages/Product';
import PageNotFound from './Pages/PageNotFound';
import Architecture from './Pages/Architecture';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Main />
      </div>
    );
  }
}

const Main = () => (
  <main>
    <Switch>
      <Route name="Home" exact path='/' component={Home}/>
      <Route name="Architecture" exact path='/architecture' component={Architecture}/>
      <Route name="Catalog" exact path='/catalog' component={Catalog}/>
      <Route exact path='/catalog/product/:id' component={Product}/>

      <Route component={PageNotFound} />
    </Switch>
  </main>
);

export default App;
