export default {
    apiEndpoints: {
        assets: process.env.REACT_APP_AssetsEndPoint,
        availability: process.env.REACT_APP_AvailabilityEndpoint,
        prices: process.env.REACT_APP_PricesEndPoint,
        products: process.env.REACT_APP_ProductsEndpoint
    }
}