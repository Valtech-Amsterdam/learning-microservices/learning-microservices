import React, { Component } from "react";
import { withRouter, Link } from 'react-router-dom';

export default withRouter(class Architecture extends Component {


  render() {
   
    return <div className="page">
      <h2>The Architecture</h2>
      <p>
        Todo...
      </p>
      <ul>
        <li>Network architecture display using icons and lines</li>
        <li>Carousel that shows activity diagram when clicked on a function icon</li>
        <li>Move over content from Medium draft</li>
        <li>Carousel that shows project setup</li>
      </ul>
    </div>
  }
});
