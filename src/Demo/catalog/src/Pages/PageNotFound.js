import React, { Component } from "react";
import { withRouter, Link } from 'react-router-dom';

export default withRouter(class PageNotFound extends Component {
  render() {
    return <div className="page">
      <p>The resource you were looking for wasn't found.</p>
      <p>Go back to the <Link to="/">Home page</Link>.</p>
    </div>;
  }
});