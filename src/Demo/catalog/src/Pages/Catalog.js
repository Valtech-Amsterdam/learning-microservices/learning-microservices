import React, { Component } from "react";
import { listProducts } from "../Util/Services/ProductService";
import ProductListItem from '../Components/Catalog/ProductListItem'
import './Catalog.css';
import { withRouter } from 'react-router-dom';
import Spinner from "../Components/Catalog/Spinner";
import queryString from 'query-string'

const pageSize = 12;

export default withRouter(class Catalog extends Component {

  constructor(props){
    super(props)

    this.state = {
      loaded: false,
      loading: false,
      expectMore: true,
      productListResponseCode: null,
      productList: [],
      page: 0
    }

    this.rootRef = React.createRef();

    this.renderProducts = this.renderProducts.bind(this)
    this.loadInitialProducts = this.loadInitialProducts.bind(this)
    this.fetchProducts = this.fetchProducts.bind(this)
    this.loadMore = this.loadMore.bind(this)
  }

  componentDidMount() {
    if(this.state.loaded) return;
    this.loadInitialProducts().then(() =>{

      this.observer = new IntersectionObserver((intersector) => {
        if(!this.state.loaded) return;
        if(this.state.loading) return;
        if(!intersector[0].isIntersecting) return;
        if(!this.state.expectMore) return;
        this.loadMore();
      }, {
        root: document.body,
        rootMargin: `0px 0px ${this.props.height || 0}px 0px`,
        threshold: 0
      });
      this.observer.observe(this.rootRef.current)
    })
  }

  async loadInitialProducts(){
    const query = queryString.parse(this.props.location.search)
    const page = !!!query.page || isNaN(query.page) ? 1 : +query.page

    for (let i = 0; i < page; i++)
      await this.fetchProducts(i)
      
    this.setState({ loaded: true })
    this.props.history.replace(`?page=${this.state.page}`)
  }

  async fetchProducts(page) {
    const skip = page * pageSize;

    const productListResponse = await listProducts(skip, pageSize)

    this.setState(state => {
      const combinedList = productListResponse.data === null ? state.productList : [...state.productList, ...productListResponse.data];
      const expectMore = productListResponse.data === null || !(productListResponse.data.length < pageSize)
      const newPage = productListResponse.data !== null && productListResponse.data.length !== 0 ? state.page +1 : state.page
      
      return {
        productListResponseCode: productListResponse.responseCode,
        loading: false,
        expectMore,
        productList: combinedList,
        page: newPage
      }
    })
  }

  async loadMore() {
    this.setState({ loading: true })
    
    await this.fetchProducts(this.state.page)
    if(this.state.page !== 0)
      this.props.history.replace(`?page=${this.state.page}`)
  }

  render() {
    return <div className="page">
      <h2>Catalog</h2>
      {this.renderProducts()}
      {this.state.loading ? <Spinner /> : null}
      {<div className="controls" ref={this.rootRef}>
        {(this.state.loaded && !this.state.loading && this.state.expectMore) ? 
          <button onClick={this.loadMore} disabled={this.state.loading}>+</button> : null}
      </div>}
    </div>
  }

  renderProducts() {
    if(!this.state.loaded)
      return <Spinner />
    
    if(this.state.productListResponseCode !== 200)
      return <p>Product listing currently not available.</p>

    if(this.state.productList.length === 0)
      return <p>There are no products in this catalog.</p>
    
    return <ul className='catalog' key="catalog">
      {this.state.productList.map(productResponse => 
        <li key={productResponse.product.id} >
          <ProductListItem product={productResponse.product} 
            pricePromise={productResponse.pricePromise} 
            assetReferencePromise={productResponse.assetReferencePromise}
            availabilityPromise={productResponse.availabilityPromise} />
        </li>)}
    </ul>
  }
});