import React, { Component } from "react";
import ProductDetail from '../Components/Catalog/ProductDetail'
import { getProductById } from '../Util/Services/ProductService'
import { ApiResponse } from '../Util/Api/WebUtils'
import './Product.css'
import { withRouter, Link } from 'react-router-dom';
import Spinner from "../Components/Catalog/Spinner";

export default withRouter(class Product extends Component {

  constructor(props){
    super(props)

    const loaded = !!props.location && !!props.location.state
    this.state = {
      loaded,
      id: props.match.params.id,
      productResponse: loaded ? new ApiResponse(200, {...props.location.state}) : null
    };
  }

  componentDidMount() {
    if(!this.state.loaded){
      getProductById(this.state.id).then(productResponse => {
        this.setState({ 
          loaded: true,
          productResponse 
        })
      })
    }
  }

  render() {
    const { loaded , productResponse } = this.state;

    if (!loaded) return  <div className="page">
      <Spinner />
    </div>

    if(productResponse.responseCode === 404) return <div className="page">
      <p>The product you were looking for wasn't found.</p>
      <p>Go back to the <Link to="/catalog">Catalog</Link>.</p>
      <p>Go back to the <Link to="/">Home page</Link>.</p>
    </div>

    if(this.state.productResponse.responseCode !== 200) return <div className="page">
      <p>Product detail currently unavailable</p>
    </div>

    return <div className="page">
      <ProductDetail  product={productResponse.data.product} 
        pricePromise={productResponse.data.pricePromise} 
        assetReferencePromise={productResponse.data.assetReferencePromise} 
        availabilityPromise={productResponse.data.availabilityPromise} />
    </div>
  }
});
