import React, { Component } from "react";
import { withRouter, Link } from 'react-router-dom';
import LazyImage from "../Components/Site/LazyImage";

// todo write the story: https://medium.com/p/a59ecc75e256/edit

export default  withRouter(class Home extends Component {
  render() {
    return <div className="page">
      <h2>About</h2>
      <p>
        For my personal goals I want to learn working with Microservices. <br />
        There are a lot of online tutorials but I figured the best way to learn is to Just do it!
      </p>
      <p>
        I learn a lot from watching YouTube tutorials or presentations about what not to do, but it's just not enough to feel I know how to build them.
      </p>
      <p className="center">
        <LazyImage url="https://media.giphy.com/media/b7f0X8Okk1uyk/giphy.gif" width={480} height={270} className="blockImage" />
      </p>
      <h3>So where to start?</h3>
      <p>
        I figured I needed a real world example so I devised a scenario that was very loosely based on a past project.
      </p>
      <p>
        The example I went for was a simplified Catalog, 
        this system not only has several external systems it's dependent upon but each system can have it's own component on the catalog itself. 
        So the usefulness of Microservices is both in "reading and writing".
      </p>
      <h3>The scenario</h3>
      <p>
        The example webshop's environment exists out of the following external services:
      </p>
      <ul>
        <li>
          A PIM that exports data into three exports: <br />
          Product data, Availability information, Asset data
        </li>
        <li>A service that provides assets</li>
        <li>A service that exports price data</li>
      </ul>
      <p>
        All export data will be stored on an ftp-server and is serialized in a somewhat &quot;exotic&quot; format, <br />
        I chose this because in my experience there's always something unique about these kinds of exports and I wanted to illustrate this functionality 
        using more than the built-in dotnet serializers.<br />
        For this reason I chose <a href="https://en.wikipedia.org/wiki/YAML" target="_blank" rel="noopener noreferrer">yml</a>.
      </p>
      <h2>The architecture</h2>
      <p>
        I wanted to think about the architecture before developing code because I didn't know anything about azure functions and I figured <br />
        Repositioning an <a href="https://en.wikipedia.org/wiki/Activity_diagram" target="_blank" rel="noopener noreferrer">activity diagram</a> was less effort 
        than refactoring code. <br />
        I started out throwing post-it's on a whiteboard mapping out the import's and api-calls:
      </p>
      <p className="center">
          <LazyImage url="/mapping-out-chaos.jpg" width={500} height={499} className="blockImage" />
          <em>The first abstract mapping of the import architecture</em>
      </p>
      <p>
        Of course I ended up refactoring a lot anyway, and of course the diagrams became stale so I had to update them at the end of the project anyway. <br />
        <Link to="architecture">Read more about the architecture</Link>.
      </p>
      <h2>The example shop</h2>
      <p>
        The demo site will be an example webshop for something abstract fully based on react. <br />
        By chance I ran into a blog post about <a href="https://www.artic.edu/" target="_blank" rel="noopener noreferrer">The art institute of Chicago</a> and 
        how they've published a lot of open domain art. Because of this I decided the demo will selling paintings.
      </p>
      <p><Link to="catalog">Browse products</Link></p>
      <p><em>
        Nothing in this demo is cached for demonstration purposes only, a real-world-example will rely on caching heavily. <br />
        Both client-side as api-side.
      </em></p>
      <h3>Disclaimer</h3>
      <p>
        I am not a frontend developer and I had no prior react experience so the react-implemenatation will be far from efficient. <br />
        Also I am aware the data model and code quality is not ideal at the moment of writing. <br/>
        This demo is about the functional use of Azure functions, however I will improve code quality in the future.
      </p>
      <p>
        At some point I might ask some collegues to make a design for this site and make a pretty frontend implemenatation but that not the goal right now.
      </p>
      <h3>The implemenatation</h3>
      <p>
        The way this webshop works is by calling the function api's directly, 
        I chose this because I wanted the site to remain operational when one or more services are down.
      </p>
      <p>
        Because every call is asynchronous and self contained I decided to make every part that uses an API actually display asynchronous as well. <br />
        The way I've implemented that is by calling all the API's early and passing allong the javascript Promise untill the moment display is requried. <br />
        This &quot;request early, resolve late&quot; provides with an implemenatation that calls all the API's at the same time but only handles the loading and display at 
        the latest stage, whether or not they are resolved.
      </p>
      <h2>Todo's</h2>
      <ul>
        <li>Order by price asc/desc then by added</li>
        <li>Fake shopping cart</li>
        <li>Fake checkout</li>
        <li>Functions based search index?</li>
        <li>Search page</li>
        <li>
          Additional business rules, like a product without a price is invalid <br />
          This needs to be fixed while importing (uses the index)
        </li>
        <li>Actual paging instead of everscroll based on index</li>
      	<li>Nice design?</li>
        <li>Icon and manifest</li>
      </ul>
      <h2>Contributors</h2>
      <ul>
        <li><a href="https://twitter.com/MarvinBrouwer" target="_blank" rel="noopener noreferrer">Marvin Brouwer</a> - Development</li>
      </ul>
      <h2>Source material</h2>
      <ul>
        <li><a href="https://www.artic.edu" target="_blank" rel="noopener noreferrer">The art institute of Chicago</a></li>
        <li><a href="https://www.microsoft.com/en-us/download/details.aspx?id=41937" target="_blank" rel="noopener noreferrer">
          Microsoft Azure, Cloud and Enterprise Symbol / Icon Set - Visio stencil, PowerPoint, PNG, SVG
        </a></li>
      </ul>
    </div>;
  }
});