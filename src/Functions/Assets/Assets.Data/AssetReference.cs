﻿using System;
using Data.Models;

namespace Assets.Data
{
    public sealed class AssetReference : IIdentity
    {
        public Uri Url { get; set; }

        public string Id { get; set; }

        public string AspectRatio { get; set; }
    }
}
