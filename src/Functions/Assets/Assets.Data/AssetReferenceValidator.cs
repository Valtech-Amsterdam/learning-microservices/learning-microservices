﻿using System.Text.RegularExpressions;
using FluentValidation;

namespace Assets.Data
{
    public sealed class AssetReferenceValidator : AbstractValidator<AssetReference>
    {
        private static readonly Regex AspectRatioPattern = new Regex(@"^(1:[1-9](\.?[0-9])*)|([1-9](\.?[0-9])*:1)*$", 
            RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);

        public AssetReferenceValidator()
        {
            RuleFor(assetReference => assetReference)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull();

            RuleFor(assetReference => assetReference.Id)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(assetReference => assetReference.AspectRatio)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .Must(ratio => AspectRatioPattern.IsMatch(ratio))
                .WithMessage("Please provide a '1:{heigth}' or '{width}:1' value!");

            RuleFor(assetReference => assetReference.Url)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .Must(url => !url.IsAbsoluteUri)
                .WithMessage("The url has to be a relative path and cannot be empty!");
        }
    }
}
