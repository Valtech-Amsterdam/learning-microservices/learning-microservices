﻿using System;
using Assets.Data;

namespace Assets.ViewModels
{
    public sealed class AssetReferenceViewModel
    {
        public Uri Url { get; set; }

        public string AspectRatio { get; set; }

        public AssetReferenceViewModel() { }
        public AssetReferenceViewModel(AssetReference model)
        {
            Url = model.Url;
            AspectRatio = model.AspectRatio;
        }
    }
}
