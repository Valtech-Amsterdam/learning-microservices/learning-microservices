using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Core.Constants;
using Core.Helpers;
using HttpClient;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace Assets.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class GetImage
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(GetImage))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = RouteConstants.Asset.GetImagePath)]
                HttpRequestMessage req,
            TraceWriter log)
        {
            // Wildcard queries can't be argument bound
            var imagePath = string.Join(string.Empty, req.RequestUri.Segments.Skip(3));
            if (string.IsNullOrWhiteSpace(imagePath)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var assetUri = RouteHelper.GetRelativeRoute(req, $"api/asset/{imagePath}", string.Empty);

                using (var inputStream = new MemoryStream())
                using (var httpClient = new RetryingHttpClient())
                using (var imageRequest = await httpClient.GetAsync(assetUri))
                {
                    if (!imageRequest.IsSuccessStatusCode) return new HttpResponseMessage(HttpStatusCode.NotFound);
                    await imageRequest.Content.CopyToAsync(inputStream).ConfigureAwait(false);
                    inputStream.Seek(0, SeekOrigin.Begin);

                    var outputStream = new MemoryStream();
                    var settings = new ImageResizer.ResizeSettings(req.RequestUri.Query);
                    ImageResizer.ImageBuilder.Current.Build(inputStream, outputStream, settings);
                    outputStream.Seek(0, SeekOrigin.Begin);

                    var response = new HttpResponseMessage
                    {
                        Content = new StreamContent(outputStream),
                    };
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(
                        $"image/{Path.GetExtension(imagePath).Trim('.')}");
                    return response;
                }
            }
            catch (Exception ex)
            {
                log.Warning(ex.Message, ex.Source);
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
