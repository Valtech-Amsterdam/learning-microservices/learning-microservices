using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Constants;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Assets.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class GetAsset
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(GetAsset))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = RouteConstants.Asset.GetAssetPath)]
                HttpRequestMessage req,
            [Blob("%Asset-BlobContainer%", Connection = "AzureWebJobsStorage")] CloudBlobContainer blobContainer,
            TraceWriter log)
        {
            // Wildcard queries can't be argument bound
            var assetPath = string.Join(string.Empty, req.RequestUri.Segments.Skip(3));
            if (string.IsNullOrWhiteSpace(assetPath)) return new HttpResponseMessage(HttpStatusCode.NotFound);
            var escapedImagePath = WebUtility.UrlDecode(assetPath);

            await blobContainer.CreateIfNotExistsAsync();

            try
            {
                var blob = await blobContainer.GetBlobReferenceFromServerAsync(escapedImagePath).ConfigureAwait(false);
                if (!await blob.ExistsAsync()) return new HttpResponseMessage(HttpStatusCode.NotFound);

                return new HttpResponseMessage
                {
                    Content = new StreamContent(await blob.OpenReadAsync().ConfigureAwait(false))
                };
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
