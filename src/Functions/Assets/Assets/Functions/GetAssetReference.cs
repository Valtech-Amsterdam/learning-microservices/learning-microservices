using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Assets.Data;
using Assets.ViewModels;
using Core.Constants;
using Core.Helpers;
using KeyVault;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using MongoDB.Driver;

namespace Assets.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class GetAssetReference
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(GetAssetReference))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = RouteConstants.Asset.GetAssetReferencePath)]
                HttpRequestMessage req, string productId,
            TraceWriter log)
        {
            if (string.IsNullOrWhiteSpace(productId)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var connectionString = await KeyVaultReader.GetSecretAsync("AssetReference-ConnectionString")
                    .ConfigureAwait(false);
                var client = new MongoClient(connectionString);
                var database = client.GetDatabase(nameof(AssetReference));
                var collection = database.GetCollection<AssetReference>(nameof(AssetReference));

                var assetReferenceResult = await collection.FindAsync(asset => asset.Id == productId)
                    .ConfigureAwait(false);
                var assetReference =  await assetReferenceResult.FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (assetReference == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

                return ResponseHelper.CreateObjectResponse(new AssetReferenceViewModel(assetReference));
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
