using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Blob;
using Reporting.Client;
using Transactions;
using Transactions.Handlers;

namespace Assets.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class ImportAssets
    {
        private static readonly string[] AssetExtensions = {
            "png", "jpg"
        };

        /// <summary>
        /// Import all asset files
        /// </summary>
        /// <param name="myTimer"></param>
        /// <param name="blobContainer"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [FunctionName(nameof(ImportAssets))] public static async Task Run(
            [TimerTrigger("%Import-Timer%")]TimerInfo myTimer, 
            [Blob("%Asset-BlobContainer%", Connection = "AzureWebJobsStorage")] CloudBlobContainer blobContainer,
            TraceWriter log)
        {
            await blobContainer.CreateIfNotExistsAsync();
            var blobFileTransaction = new FtpToBlobHandler(blobContainer);
            var reportClient = await ReportClient.CreateAsync("Asset import", log).ConfigureAwait(false);
            var transactionManager = new FtpTransaction(log, reportClient, blobFileTransaction);
            var ftpDir = Environment.GetEnvironmentVariable("Asset-BaseDirectory");

            await transactionManager.Transfer(ftpDir, AssetExtensions).ConfigureAwait(false);
        }
    }
}
