using System;
using System.Threading.Tasks;
using Assets.Data;
using Core.Constants;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Reporting.Client;
using Transactions;
using Transactions.Handlers;

namespace Assets.Functions
{
    public static class ImportAssetReferences
    {
        /// <summary>
        /// Import all asset files
        /// </summary>
        [FunctionName(nameof(ImportAssetReferences))] public static async Task Run(
            [TimerTrigger("%Import-Timer%")]TimerInfo myTimer,
            [Queue(nameof(AssetReference))] CloudQueue queue,
            TraceWriter log)
        {
            await queue.CreateIfNotExistsAsync();
            var validator = new AssetReferenceValidator();
            var queueTransaction = new FtpToQueueHandler<AssetReference>(log, queue, validator);

            var reportClient = await ReportClient.CreateAsync("Asset reference import", log).ConfigureAwait(false);
            var transactionManager = new FtpTransaction(log, reportClient, queueTransaction);
            var ftpDir = Environment.GetEnvironmentVariable("AssetReference-BaseDirectory");
            
            await transactionManager.Transfer(ftpDir, ImportConstants.DataImportFileExtension).ConfigureAwait(false);
        }
    }
}
