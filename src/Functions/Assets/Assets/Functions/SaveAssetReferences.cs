using System.Threading.Tasks;
using Assets.Data;
using Data.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Reporting.Client;
using Transactions;

namespace Assets.Functions
{
    public static class SaveAssetReferences
    {
        [FunctionName(nameof(SaveAssetReferences))] public static async Task Run(
            [QueueTrigger(nameof(AssetReference))] UpdateCommand<AssetReference> importLine,
            TraceWriter log)
        {
            var reportClient = await ReportClient.CreateAsync("AssetReference import", log).ConfigureAwait(false);
            var transaction = new DbTransaction<AssetReference>(log, reportClient, "AssetReference-ConnectionString");

            await transaction.Transfer(importLine, new AssetReferenceValidator());
        }
    }
}
