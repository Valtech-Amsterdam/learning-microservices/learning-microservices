﻿namespace Products.Data
{
    public enum FrameType
    {
        NoFrame,
        Classic,
        ModernSlick,
        ModernThick
    }
}
