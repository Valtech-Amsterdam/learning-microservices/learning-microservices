﻿using System.Collections.Generic;
using Data.Models;

namespace Products.Data
{
    public sealed class Product : IIdentity
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public PaintingType Type { get; set; }
        public IEnumerable<FrameType> Frames { get; set; }

        // todo: can be a separate database
        public string Artist { get; set; }

        public string Description { get; set; }
    }
}
