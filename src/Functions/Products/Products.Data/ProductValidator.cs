﻿using System.Linq;
using FluentValidation;

namespace Products.Data
{
    public sealed class ProductValidator : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(product => product)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull();

            RuleFor(product => product.Id)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
            
            RuleFor(product => product.Name)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(product => product.Description)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(product => product.Artist)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(product => product.Type)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .IsInEnum();

            RuleFor(product => product.Frames)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .Must(frames => frames.Any())
                .WithMessage("You need to specify at leas one frame type");
            RuleForEach(product => product.Frames)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .IsInEnum();
        }
    }
}
