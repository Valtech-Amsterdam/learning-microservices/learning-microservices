﻿namespace Products.Data
{
    public enum PaintingType
    {
        OilPainting,
        WaterColour,
        InkWash,
        Acrylic,
        Pastel,
        Glass,
        Fresco,
        Encaustic,
        Gouache,
        SprayPainting,
        Tempera,
        Digital,
        Sand,
        Collage,
        Kalamkari
    }
}
