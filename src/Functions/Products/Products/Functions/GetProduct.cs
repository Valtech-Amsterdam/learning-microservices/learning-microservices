using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Constants;
using Core.Helpers;
using KeyVault;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using MongoDB.Driver;
using Products.ViewModels;

namespace Products.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class GetProduct
    {
        /// <summary>
        /// Retrieve a Product
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(GetProduct))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = RouteConstants.Product.GetProductPath)]
                HttpRequestMessage req, string productId, 
            TraceWriter log)
        {
            if (string.IsNullOrWhiteSpace(productId)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var connectionString = await KeyVaultReader.GetSecretAsync("Product-ConnectionString")
                    .ConfigureAwait(false);
                var client = new MongoClient(connectionString);
                var database = client.GetDatabase(nameof(Data.Product));
                var collection = database.GetCollection<Data.Product>(nameof(Data.Product));

                var productResult = await collection.FindAsync(pr => pr.Id == productId)
                    .ConfigureAwait(false);
                var product = await productResult.FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (product == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

                return ResponseHelper.CreateObjectResponse(new ProductViewModel(product));
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
