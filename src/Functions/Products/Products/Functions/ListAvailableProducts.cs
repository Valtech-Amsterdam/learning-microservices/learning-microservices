using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Constants;
using Core.Extensions;
using Core.Helpers;
using Data.Models;
using HttpClient;
using KeyVault;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using MongoDB.Driver;
using Products.Data;

namespace Products.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class ListAvailableProducts
    {
        /// <summary>
        /// Retrieve all products
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(ListAvailableProducts))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = RouteConstants.Product.ListAvailableProductsPath)]
                HttpRequestMessage req,
            TraceWriter log)
        {
            var queryParams = req.GetQueryNameValuePairs().ToList();
            var skipParam = queryParams.FirstOrDefault(q => q.Key == "skip").Value;
            if (string.IsNullOrWhiteSpace(skipParam) || !int.TryParse(skipParam, out var skip))
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The skip parameter is required!")
                };
            var takeParam = queryParams.FirstOrDefault(q => q.Key == "take").Value;
            if (string.IsNullOrWhiteSpace(takeParam) || !int.TryParse(takeParam, out var take))
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The take parameter is required!")
                };

            try
            {
                var connectionString = await KeyVaultReader.GetSecretAsync("Product-ConnectionString")
                    .ConfigureAwait(false);
                var client = new MongoClient(connectionString);
                var database = client.GetDatabase(nameof(Product));
                var productEntityCollection = database.GetCollection<DataEntity>(nameof(DataEntity));
                var productCollection = database.GetCollection<Product>(nameof(Product));

                var productResult = await productEntityCollection.FindAsync(_ => true)
                    .ConfigureAwait(false);
                var productIds = productResult?
                    .ToEnumerable()?
                    .Where(productEntity => IsAvailable(productEntity.Id).GetResultSynchronously())
                    .OrderBy(product => product.AddedDate)
                    .Select(productEntity => productEntity.Id)
                    .Skip(skip)
                    .Take(take)
                    .ToList();
                if (productIds == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

                var productViewModels = await BuildProductViewModelsAsync(productCollection, productIds);
                return ResponseHelper.CreateObjectResponse(productViewModels);
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        private static ConfiguredTaskAwaitable<Product[]> BuildProductViewModelsAsync(
            IMongoCollection<Product> productCollection, List<string> productIds)
        {
            return Task.WhenAll(productIds.Select(async productId =>
                {
                    var productQuery = await productCollection.FindAsync(p => p.Id == productId).ConfigureAwait(false);

                    return await productQuery.FirstOrDefaultAsync().ConfigureAwait(false);
                }))
                .ConfigureAwait(false);
        }

        private static async Task<bool> IsAvailable(string productId)
        {
            var isAvailableUrl = RouteHelper.GetAbsolutePath("Availability", $"available/{productId}");

            using (var httpClient = new RetryingHttpClient())
            using (var availabilityRequest = await httpClient.GetAsync(isAvailableUrl))
            {
                if (!availabilityRequest.IsSuccessStatusCode) return false;

                return await availabilityRequest.Content.ReadAsAsync<bool>()
                    .ConfigureAwait(false);
            }
        }
    }
}
