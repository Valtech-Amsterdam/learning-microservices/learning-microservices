﻿using System.Threading.Tasks;
using Data.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Products.Data;
using Reporting.Client;
using Transactions;

namespace Products.Functions
{
    public static class SaveProducts
    {
        [FunctionName(nameof(SaveProducts))]
        public static async Task Run(
            [QueueTrigger(nameof(Product))] UpdateCommand<Product> importLine,
            TraceWriter log)
        {
            var reportClient = await ReportClient.CreateAsync("Product import", log).ConfigureAwait(false);
            var transaction = new DbTransaction<Product>(log, reportClient, "Product-ConnectionString");

            await transaction.Transfer(importLine, new ProductValidator());
        }
    }
}
