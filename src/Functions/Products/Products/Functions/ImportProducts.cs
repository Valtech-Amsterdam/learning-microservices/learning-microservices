using System;
using System.Threading.Tasks;
using Core.Constants;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Products.Data;
using Reporting.Client;
using Transactions;
using Transactions.Handlers;

namespace Products.Functions
{
    public static class ImportProducts
    {
        /// <summary>
        /// Import all product files
        /// </summary>
        [FunctionName(nameof(ImportProducts))] public static async Task Run(
            [TimerTrigger("%Import-Timer%")] TimerInfo myTimer,
            [Queue(nameof(Product))] CloudQueue queue,
            TraceWriter log)
        {
            await queue.CreateIfNotExistsAsync();
            var validator = new ProductValidator();
            var queueTransaction = new FtpToQueueHandler<Product>(log, queue, validator);

            var reportClient = await ReportClient.CreateAsync("Product import", log).ConfigureAwait(false);
            var transactionManager = new FtpTransaction(log, reportClient, queueTransaction);
            var ftpDir = Environment.GetEnvironmentVariable("Product-BaseDirectory");

            await transactionManager.Transfer(ftpDir, ImportConstants.DataImportFileExtension).ConfigureAwait(false);
        }
    }
}
