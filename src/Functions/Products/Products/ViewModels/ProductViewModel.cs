﻿using System.Collections.Generic;
using Products.Data;

namespace Products.ViewModels
{
    public sealed class ProductViewModel
    {
        public string Id { get; }

        public string Name { get; }

        public PaintingType Type { get; }
        public IEnumerable<FrameType> Frames { get; }

        public string Artist { get; }

        public string Description { get; }

        public ProductViewModel() { }
        public ProductViewModel(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Type = product.Type;
            Frames = product.Frames;
            Artist = product.Artist;
            Description = product.Description;
        }

    }
}
