﻿using FluentValidation;

namespace Prices.Data
{
    public sealed class PriceValueValidator : AbstractValidator<PriceValue>
    {
        public PriceValueValidator()
        {
            RuleFor(priceValue => priceValue)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull();

            RuleFor(priceValue => priceValue.StartDate)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(priceValue => priceValue.Cents)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
        }
    }
}
