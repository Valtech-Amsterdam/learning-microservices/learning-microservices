﻿using System.Collections.Generic;
using Data.Models;

namespace Prices.Data
{
    public sealed class Price : IIdentity
    {
        public string Id { get; set; }

        public IEnumerable<PriceValue> Prices { get; set; }
    }
}
