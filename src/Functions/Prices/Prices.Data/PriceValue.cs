﻿using System;

namespace Prices.Data
{
    public sealed class PriceValue
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public long Cents { get; set; } 
    }
}
