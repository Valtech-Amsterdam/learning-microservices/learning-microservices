﻿using FluentValidation;

namespace Prices.Data
{
    public sealed class PriceValidator : AbstractValidator<Price>
    {
        public PriceValidator()
        {
            RuleFor(price => price)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull();

            RuleFor(price => price.Id)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleForEach(price => price.Prices)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .SetValidator(new PriceValueValidator());
        }
    }
}
