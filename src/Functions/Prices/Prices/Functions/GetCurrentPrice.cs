using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Constants;
using Core.Helpers;
using HttpClient;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;

namespace Prices.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class GetCurrentPrice
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(GetCurrentPrice))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = RouteConstants.Price.GetCurrentPricePath)]
                HttpRequestMessage req, string productId,
            TraceWriter log)
        {
            if (string.IsNullOrWhiteSpace(productId)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var pricesUrl = RouteHelper.GetRelativeRoute(req, $"api/price/all/{productId}");
                using (var httpClient = new RetryingHttpClient())
                using (var pricesRequest = await httpClient.GetAsync(pricesUrl))
                {
                    if (!pricesRequest.IsSuccessStatusCode) return new HttpResponseMessage(HttpStatusCode.NotFound);
                    var prices = await pricesRequest.Content.ReadAsAsync<IEnumerable<Data.PriceValue>>()
                        .ConfigureAwait(false);

                    var currentPrice = prices?.FirstOrDefault(
                        price => price.StartDate.ToUniversalTime() <= DateTime.UtcNow && (
                                 price.EndDate == null || price.EndDate?.ToUniversalTime() > DateTime.UtcNow));

                    if (currentPrice == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

                    return ResponseHelper.CreateObjectResponse(currentPrice.Cents);
                }
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
