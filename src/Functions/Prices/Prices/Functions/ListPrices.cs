using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Constants;
using Core.Helpers;
using KeyVault;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using MongoDB.Driver;

namespace Prices.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class ListPrices
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(ListPrices))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = RouteConstants.Price.ListPricesPath)]
                HttpRequestMessage req, string productId, 
            TraceWriter log)
        {
            if (string.IsNullOrWhiteSpace(productId)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var connectionString = await KeyVaultReader.GetSecretAsync("Price-ConnectionString")
                    .ConfigureAwait(false);
                var client = new MongoClient(connectionString);
                var database = client.GetDatabase(nameof(Data.Price));
                var collection = database.GetCollection<Data.Price>(nameof(Data.Price));

                var productPriceResult = await collection.FindAsync(pr => pr.Id == productId)
                    .ConfigureAwait(false);
                var price = await productPriceResult.FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (price == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

                return ResponseHelper.CreateObjectResponse(price.Prices);
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
