using System.Threading.Tasks;
using Data.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Prices.Data;
using Reporting.Client;
using Transactions;

namespace Prices.Functions
{
    public static class SavePrices
    {
        [FunctionName(nameof(SavePrices))] public static async Task Run(
            [QueueTrigger(nameof(Price))] UpdateCommand<Price> importLine,
            TraceWriter log)
        {
            var reportClient = await ReportClient.CreateAsync("Price import", log).ConfigureAwait(false);
            var transaction = new DbTransaction<Price>(log, reportClient, "Price-ConnectionString");

            await transaction.Transfer(importLine, new PriceValidator());
        }
    }
}
