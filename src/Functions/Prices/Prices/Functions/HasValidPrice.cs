using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Constants;
using Core.Helpers;
using HttpClient;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;

namespace Prices.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class HasValidPrice
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(HasValidPrice))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = RouteConstants.Price.HasValidPricePath)]
                HttpRequestMessage req, string productId,
            TraceWriter log)
        {
            if (string.IsNullOrWhiteSpace(productId)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var currentPriceUrl = RouteHelper.GetRelativeRoute(req, $"api/price/current/{productId}");
                using (var httpClient = new RetryingHttpClient())
                using (var availabilityRequest = await httpClient.GetAsync(currentPriceUrl))
                {
                    if (!availabilityRequest.IsSuccessStatusCode) return ResponseHelper.CreateObjectResponse(false);

                    var currentPrice = await availabilityRequest.Content.ReadAsAsync<long?>()
                        .ConfigureAwait(false);

                    return ResponseHelper.CreateObjectResponse(currentPrice != null);
                }
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return ResponseHelper.CreateObjectResponse(false);
            }
        }
    }
}
