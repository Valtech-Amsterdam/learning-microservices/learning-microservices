using System;
using System.Threading.Tasks;
using Core.Constants;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Prices.Data;
using Reporting.Client;
using Transactions;
using Transactions.Handlers;

namespace Prices.Functions
{
    public static class ImportPrices
    {
        /// <summary>
        /// Import all price files
        /// </summary>
        [FunctionName(nameof(ImportPrices))] public static async Task Run(
            [TimerTrigger("%Import-Timer%")] TimerInfo myTimer,
            [Queue(nameof(Price))] CloudQueue queue,
            TraceWriter log)
        {
            await queue.CreateIfNotExistsAsync();
            var validator = new PriceValidator();
            var queueTransaction = new FtpToQueueHandler<Price>(log, queue, validator);

            var reportClient = await ReportClient.CreateAsync("Price import", log).ConfigureAwait(false);
            var transactionManager = new FtpTransaction(log, reportClient, queueTransaction);
            var ftpDir = Environment.GetEnvironmentVariable("Price-BaseDirectory");

            await transactionManager.Transfer(ftpDir, ImportConstants.DataImportFileExtension).ConfigureAwait(false);
        }
    }
}
