﻿using System;

namespace Availability.ViewModels
{
    public sealed class AvailabilityViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public AvailabilityViewModel() { }
        public AvailabilityViewModel(Data.Availability availability)
        {
            StartDate = availability.StartDate;
            EndDate = availability.EndDate;
        }
    }
}
