using System.Threading.Tasks;
using Availability.Data;
using Data.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Reporting.Client;
using Transactions;

namespace Availability.Functions
{
    public static class SaveAvailabilities
    {
        [FunctionName(nameof(SaveAvailabilities))] public static async Task Run(
            [QueueTrigger(nameof(Data.Availability))] UpdateCommand<Data.Availability> importLine,
            TraceWriter log)
        {
            var reportClient = await ReportClient.CreateAsync("Availability import", log).ConfigureAwait(false);
            var transaction = new DbTransaction<Data.Availability>(log, reportClient, "Availability-ConnectionString");

            await transaction.Transfer(importLine, new AvailabilityValidator());
        }
    }
}
