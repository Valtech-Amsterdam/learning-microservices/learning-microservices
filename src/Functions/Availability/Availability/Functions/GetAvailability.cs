using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Availability.ViewModels;
using Core.Constants;
using Core.Helpers;
using KeyVault;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using MongoDB.Driver;

namespace Availability.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class GetAvailability
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(GetAvailability))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = RouteConstants.Availability.GetAvailabilityPath)]
                HttpRequestMessage req,
            TraceWriter log)
        {
            var productId = req.RequestUri.Segments.Skip(3).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(productId)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var connectionString = await KeyVaultReader.GetSecretAsync("Availability-ConnectionString")
                    .ConfigureAwait(false);
                var client = new MongoClient(connectionString);
                var database = client.GetDatabase(nameof(Data.Availability));
                var collection = database.GetCollection<Data.Availability>(nameof(Data.Availability));

                var availabilityResult = await collection.FindAsync(av => av.Id == productId)
                    .ConfigureAwait(false);
                var availability = await availabilityResult.FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (availability == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

                return ResponseHelper.CreateObjectResponse(new AvailabilityViewModel(availability));
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
