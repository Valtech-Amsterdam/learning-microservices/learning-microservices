using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Availability.ViewModels;
using Core.Constants;
using Core.Helpers;
using HttpClient;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;

namespace Availability.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class IsAvailable
    {
        /// <summary>
        /// Retrieve an asset from blob storage
        /// </summary>
        /// <returns></returns>
        [FunctionName(nameof(IsAvailable))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = RouteConstants.Availability.IsAvailablePath)]
                HttpRequestMessage req, string productId,
            TraceWriter log)
        {
            if (string.IsNullOrWhiteSpace(productId)) return new HttpResponseMessage(HttpStatusCode.NotFound);

            try
            {
                var availabilityUrl = RouteHelper.GetRelativeRoute(req, $"api/availabilty/{productId}");
                using (var httpClient = new RetryingHttpClient())
                using (var availabilityRequest = await httpClient.GetAsync(availabilityUrl))
                {
                    if (!availabilityRequest.IsSuccessStatusCode) return new HttpResponseMessage(HttpStatusCode.NotFound);
                    var availability = await availabilityRequest.Content.ReadAsAsync<AvailabilityViewModel>()
                        .ConfigureAwait(false);

                    var isAvailable = availability?.StartDate.ToUniversalTime() <= DateTime.UtcNow && 
                        (availability.EndDate == null || availability.EndDate?.ToUniversalTime() > DateTime.UtcNow);

                    return ResponseHelper.CreateObjectResponse(isAvailable);
                }
            }
            catch (StorageException ex) when (ex.InnerException is WebException webEx && webEx.Message.Contains("Not Found"))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
