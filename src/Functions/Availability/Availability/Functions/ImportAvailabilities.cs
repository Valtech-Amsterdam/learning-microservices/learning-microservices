using System;
using System.Threading.Tasks;
using Availability.Data;
using Core.Constants;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Reporting.Client;
using Transactions;
using Transactions.Handlers;

namespace Availability.Functions
{
    public static class ImportAvailabilities
    {
        /// <summary>
        /// Import all availability files
        /// </summary>
        [FunctionName(nameof(ImportAvailabilities))] public static async Task Run(
            [TimerTrigger("%Import-Timer%")]TimerInfo myTimer,
            [Queue(nameof(Data.Availability))] CloudQueue queue,
            TraceWriter log)
        {
            await queue.CreateIfNotExistsAsync();
            var validator = new AvailabilityValidator();
            var queueTransaction = new FtpToQueueHandler<Data.Availability>(log, queue, validator);

            var reportClient = await ReportClient.CreateAsync("Availability import", log).ConfigureAwait(false);
            var transactionManager = new FtpTransaction(log, reportClient, queueTransaction);
            var ftpDir = Environment.GetEnvironmentVariable("Availability-BaseDirectory");

            await transactionManager.Transfer(ftpDir, ImportConstants.DataImportFileExtension).ConfigureAwait(false);
        }
    }
}
