﻿using System;
using Data.Models;

namespace Availability.Data
{
    public sealed class Availability : IIdentity
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Id { get; set; }
    }
}
