﻿using FluentValidation;

namespace Availability.Data
{
    public sealed class AvailabilityValidator : AbstractValidator<Availability>
    {
        public AvailabilityValidator()
        {
            RuleFor(assetReference => assetReference)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull();

            RuleFor(assetReference => assetReference.Id)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(assetReference => assetReference.StartDate)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
        }
    }
}
