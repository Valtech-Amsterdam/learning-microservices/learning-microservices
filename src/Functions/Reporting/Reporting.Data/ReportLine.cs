﻿using System;

namespace Reporting.Data
{
    /// <summary>
    /// Message to add to the next batch of reports
    /// </summary>
    public sealed class ReportLine
    {
        /// <summary>
        /// The category this message belongs to, usualy the name of the Microservice
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// The message used for the reporting line in the email
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The type of report this is
        /// </summary>
        public ReportType ReportType { get; set; }

        /// <summary>
        /// The time the reported scenario occured
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}