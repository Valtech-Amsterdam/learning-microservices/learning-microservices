﻿namespace Reporting.Data
{
    /// <summary>
    /// Types of reporting supported by the Reporting Microservice
    /// </summary>
    public enum ReportType
    {
        /// <summary>
        /// One of the validation rules specified by the business didn't comply
        /// </summary>
        ValidationError,

        /// <summary>
        /// The data imported has an invalid structure or is missing essential fields
        /// </summary>
        DataError,

        /// <summary>
        /// An exception occured
        /// </summary>
        Exception
    }
}
