﻿using FluentValidation;

namespace Reporting.Data
{
    public sealed class ReportLineValidator : AbstractValidator<ReportLine>
    {
        public ReportLineValidator()
        {
            RuleFor(line => line)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull();

            RuleFor(line => line.ReportType)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .IsInEnum();

            RuleFor(line => line.Category)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(line => line.Message)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(line => line.Timestamp)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
        }
    }
}
