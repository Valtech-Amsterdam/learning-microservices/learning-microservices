﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Core.Extensions;
using KeyVault;
using Microsoft.Azure.WebJobs.Host;
using Queue;
using Reporting.Data;
using SendGrid;

namespace Reporting.MailMessage
{
    /// <inheritdoc />
    public sealed class MessageSender
    {
        private readonly MessageBuilder _messageBuilder;
        private readonly SendGridClient _sendGridClient;
        private readonly TraceWriter _log;

        /// <summary>
        /// Send an HTML email containing the reportLines for the category
        /// </summary>
        public MessageSender(TraceWriter log, string executionDirectory)
        {
            var sendGridAuthKey = KeyVaultReader
                .GetSecretAsync("SendGrid-AuthKey")
                .GetResultSynchronously();

            _sendGridClient = new SendGridClient(sendGridAuthKey);
            _messageBuilder = new MessageBuilder(executionDirectory);

            _log = log;
        }

        /// <summary>
        /// Send an HTML email containing the reportLines for the category
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="category"></param>
        /// <param name="reportLines"></param>
        /// <returns></returns>
        public async Task<bool> SendReportForCategory(DateTime timestamp, string category, List<CloudQueuedItem<ReportLine>> reportLines)
        {
            var message = _messageBuilder.BuildReportMessage(timestamp, category, reportLines);
            var response = await _sendGridClient.SendEmailAsync(message);

            // Check if status code is a 200 response
            var success = ((int)response.StatusCode % (int)HttpStatusCode.OK) < 100;

            if (success) return true;

            _log.Error($"An error occured while sending to SendGrid: {await response.Body.ReadAsStringAsync()}");
            return false;
        }
    }
}
