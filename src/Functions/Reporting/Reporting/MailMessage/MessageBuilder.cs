﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Queue;
using Reporting.Data;
using SendGrid.Helpers.Mail;

namespace Reporting.MailMessage
{
    /// <inheritdoc />
    public sealed class MessageBuilder
    {
        private readonly EmailAddress _fromAddress;
        private readonly EmailAddress _toAddress;
        private readonly string _mailStyleSheet;

        /// <summary>
        /// This converts the reportLines for a category to a <see cref="SendGridMessage" />
        /// </summary>
        public MessageBuilder(string executionDirectory)
        {
            _fromAddress = new EmailAddress(Environment.GetEnvironmentVariable("SendGrid-FromAddress"));
            _toAddress = new EmailAddress(Environment.GetEnvironmentVariable("SendGrid-ToAddress"));
            
            var mailStylePath = Path.Combine(executionDirectory, "MailMessage", "Report.css");
            using (var fileReader = new FileInfo(mailStylePath).OpenRead())
            using (var styleReader = new StreamReader(fileReader))
                _mailStyleSheet = styleReader.ReadToEnd();
        }

        /// <summary>
        /// This converts the reportLines for a category to a <see cref="SendGridMessage" />
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="category"></param>
        /// <param name="reportLines"></param>
        /// <returns></returns>
        public SendGridMessage BuildReportMessage(DateTime timestamp, string category,
            List<CloudQueuedItem<ReportLine>> reportLines)
        {
            var title = $"Report for [{category}] at {timestamp:dd-MM-yyyy}";
            var message = new SendGridMessage
            {
                From = _fromAddress,
                Subject = title,
                HtmlContent = BuildBody(reportLines, title)
            };
            message.AddTo(_toAddress);

            return message;
        }

        private string BuildBody(List<CloudQueuedItem<ReportLine>> reportLines, string title)
        {
            var body = new StringBuilder();
            body.AppendLine("<html>");
            body.AppendLine(  "<head>");
            body.AppendLine(    $"<style>{_mailStyleSheet}</style>");
            body.AppendLine(  "</head>");
            body.AppendLine(  "<body>");
            body.AppendLine(    $"<h1>{title}</h1>");
            body.AppendLine(      BuildReportContent(reportLines));
            body.AppendLine(  "</body>");
            body.AppendLine("</html>");
            return body.ToString();
        }

        private static string BuildReportContent(List<CloudQueuedItem<ReportLine>> reportLines)
        {
            var reportLinesByType = reportLines
                .GroupBy(report => report.Item.ReportType)
                .OrderByDescending(group => group.Key);

            var stringBuilder = new StringBuilder();
            foreach (var reportTypeGroup in reportLinesByType)
            {
                stringBuilder.AppendLine($"<h2 class=\"{reportTypeGroup.Key}\">{GetKeyName(reportTypeGroup)}</h2>");
                stringBuilder.AppendLine("<ul>");
                foreach (var reportLine in reportTypeGroup)
                {
                    stringBuilder.AppendLine("<li>");
                    stringBuilder.AppendLine(  $"<strong>[{reportLine.Item.Timestamp:HH:mm}] </strong>");
                    stringBuilder.AppendLine(    reportLine.Item.Message);
                    stringBuilder.AppendLine("</li>");
                }
                stringBuilder.AppendLine("</ul>");
            }

            return stringBuilder.ToString();
        }

        private static string GetKeyName(IGrouping<ReportType, CloudQueuedItem<ReportLine>> reportTypeGroup)
        {
            switch (reportTypeGroup.Key)
            {
                case ReportType.ValidationError: return "Validation errors";
                case ReportType.DataError: return "Data errors";
                case ReportType.Exception: return "Unhandled exceptions";

                // This should never happen because of model validation before enqueueing
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}
