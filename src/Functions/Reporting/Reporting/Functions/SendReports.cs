using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Queue;
using Reporting.MailMessage;
using Reporting.Data;

namespace Reporting.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class SendReports
    {
        /// <summary>
        /// Azure function for sending all queued reports to SendGrid
        /// </summary>
        /// <param name="myTimer"></param>
        /// <param name="reportQueue"></param>
        /// <param name="log"></param>
        /// <param name="executionContext"></param>
        /// <returns></returns>
        [FunctionName(nameof(SendReports))] public static async Task Run(
            [TimerTrigger("%Report-Timer%")] TimerInfo myTimer,
            [Queue("%Queue-ReportingQueueName%")] CloudQueue reportQueue,
            TraceWriter log, ExecutionContext executionContext)
        {
            log.Info($"[{DateTime.UtcNow}] Reading queued reports");

            var reports = await QueueReader.ReadCloudQueue<ReportLine>(reportQueue).ConfigureAwait(false);
            var reportsPerDate = reports
                .GroupBy(report => report.Item.Timestamp)
                .Select(report => new
                {
                    Timestamp = report.Key,
                    ReportLines = report.AsEnumerable()
                })
                .ToList();

            log.Info($"Read reports for {reportsPerDate.Count} dates");
            if (reportsPerDate.Any()) 
                foreach (var dateReports in reportsPerDate)
                    await HandleReportsforDate(dateReports.Timestamp, dateReports.ReportLines.ToList(), reportQueue, log, executionContext.FunctionAppDirectory)
                        .ConfigureAwait(false);

            log.Info("Finished sending reports");
        }

        private static async Task HandleReportsforDate(DateTime timestamp, List<CloudQueuedItem<ReportLine>> reportLines, CloudQueue reportQueue,
            TraceWriter log, string executionDirectory)
        {
            if (!reportLines.Any()) return;

            var messageSender = new MessageSender(log, executionDirectory);
            var reportsPerCategory = reportLines
                .GroupBy(report => report.Item.Category)
                .Select(report => new
                {
                    Category = report.Key,
                    ReportLines = report.AsEnumerable()
                });

            log.Info("Sending reports");
            foreach (var categoryReport in reportsPerCategory)
            {
                var categpryReportLines = categoryReport.ReportLines.ToList();
                var mailSent = await messageSender.SendReportForCategory(timestamp, categoryReport.Category, categpryReportLines);

                if (!mailSent) continue;
                foreach (var queuedItem in categpryReportLines)
                    await reportQueue.DeleteMessageAsync(queuedItem.Id, queuedItem.PopReceipt).ConfigureAwait(false);
            }
        }
    }
}
