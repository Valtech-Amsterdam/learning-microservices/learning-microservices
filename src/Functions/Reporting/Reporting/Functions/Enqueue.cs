using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Queue;
using Queue;
using Reporting.Data;

namespace Reporting.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class Enqueue
    {
        /// <summary>
        /// Add a new report entry to the queue
        /// </summary>
        /// <param name="report"></param>
        /// <param name="reportQueue"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [FunctionName(nameof(Enqueue))] public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] ReportLine report,
            [Queue("%Queue-ReportingQueueName%")] CloudQueue reportQueue,
            TraceWriter log)
        {
            var enqueuer = new ValidatedCloudEnqueuer<ReportLine>(log, reportQueue, new ReportLineValidator());
            return await enqueuer.Enqueue(report).ConfigureAwait(false);
        }
    }
}
