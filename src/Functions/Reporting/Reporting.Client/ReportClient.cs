﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using HttpClient;
using KeyVault;
using Microsoft.Azure.WebJobs.Host;
using Reporting.Data;

namespace Reporting.Client
{
    public sealed class ReportClient
    {
        private readonly string _categoryName;
        private readonly TraceWriter _log;
        private readonly string _functionAuthKey;
        private readonly string _endpoint;

        private ReportClient(string categoryName, TraceWriter log, string endpoint, string functionAuthKey)
        {
            _categoryName = categoryName;
            _log = log;
            _functionAuthKey = functionAuthKey;
            _endpoint = endpoint;
        }

        public static async Task<ReportClient> CreateAsync(string categoryName, TraceWriter log)
        {
            var endPoint = await KeyVaultReader.GetSecretAsync("Reporting-Endpoint").ConfigureAwait(false);
            var functionAuthKey = await KeyVaultReader.GetSecretAsync("Reporting-AuthKey").ConfigureAwait(false);

            return new ReportClient(categoryName, log, endPoint, functionAuthKey);
        }

        private async Task Report(string message, ReportType type)
        {
            var shortMessage = message.Length <= 100 ? message : $"{message.Substring(0, 97)}..."; 
            _log.Error($"Sending error report for '{_categoryName}': {shortMessage}");

            var reportLine = new ReportLine
            {
                Category = _categoryName,
                Message = message
                    .Replace("\n   ", "&nbsp; &nbsp;")
                    .Replace("\n", "<br />"),
                Timestamp = DateTime.UtcNow,
                ReportType = type
            };

            using (var httpClient = new RetryingHttpClient(_endpoint))
            {
                var response = await httpClient.PostAsync($"api/Enqueue?code={_functionAuthKey}", reportLine);
                if (response.IsSuccessStatusCode) return;

                _log.Error($"Reporting unsuccesfull: '{response.ReasonPhrase}'");
            }
        }

        public ConfiguredTaskAwaitable ReportValidationError(string message) 
            => Report(message, ReportType.ValidationError).ConfigureAwait(false);
        public ConfiguredTaskAwaitable ReportDataError(string message) 
            => Report(message, ReportType.DataError).ConfigureAwait(false);
        public ConfiguredTaskAwaitable ReportException(string message) 
            => Report(message, ReportType.Exception).ConfigureAwait(false);
    }
}
