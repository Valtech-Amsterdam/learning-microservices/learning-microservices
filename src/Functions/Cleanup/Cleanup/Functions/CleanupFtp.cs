using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.Constants;
using Ftp;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Reporting.Client;

namespace Cleanup.Functions
{
    /// <inheritdoc cref="Run"/>
    public static class CleanupFtp
    {
        /// <summary>
        /// Import all asset files
        /// </summary>
        /// <param name="myTimer"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [FunctionName(nameof(CleanupFtp))] public static async Task Run(
            [TimerTrigger("%Import-Timer%")]TimerInfo myTimer, 
            TraceWriter log)
        {
            // During debug just use one FTP server on the root folder but deploy a separate cleanup job for every type of import
            // in other environments
            var cleanupName = Environment.GetEnvironmentVariable("Cleanup-Name");
            var reportClient = await ReportClient.CreateAsync(cleanupName, log).ConfigureAwait(false);
            
            try
            {
                var staleOffsetInDays = Convert.ToInt32(Environment.GetEnvironmentVariable("Cleanup-StaleOffsetInDays"));
                var nowTicks = DateTime.UtcNow.AddDays(-staleOffsetInDays).Ticks;
                var ftpDir = Environment.GetEnvironmentVariable("Cleanup-BaseDirectory");
                var ftpConnectionFactory = new FtpConnectionFactory();

                log.Info("Starting ftp cleanup");
                using (var transaction = await ftpConnectionFactory
                    .CreateConnection(reportClient, ftpDir).ConfigureAwait(false))

                    await TraverseDirectories(log, transaction, nowTicks).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                await reportClient.ReportException($"An error occured while cleaning up the import files: \n{ex}");
                throw;
            }
        }

        private static async Task TraverseDirectories(TraceWriter log, FtpConnection connection, 
            long nowTicks, string workingDirectory = null)
        {
            if (!string.IsNullOrEmpty(workingDirectory))
                await connection.ChangeWorkingDirectoryAsync(workingDirectory);

            var directories = await connection.ListDirectoriesAsync();
            foreach (var directory in directories)
            {
                if (directory.Name.Equals(ImportConstants.FailedFolderName))
                    await CleanOldFiles(log, connection, workingDirectory, nowTicks, ImportConstants.FailedFolderName);
                else if (directory.Name.Equals(ImportConstants.ProcessedFolderName))
                    await CleanOldFiles(log, connection, workingDirectory, nowTicks, ImportConstants.ProcessedFolderName);
                else
                    await TraverseDirectories(log, connection, nowTicks, directory.Name);
            }

            await connection.ChangeWorkingDirectoryAsync("../");
        }

        private static async Task CleanOldFiles(TraceWriter log, FtpConnection transaction, 
            string workingDirectory, long nowTicks, string directory)
        {
            await transaction.ChangeWorkingDirectoryAsync(directory);
            var staleFiles = (await transaction.ListFilesAsync())
                .Where(file => GetTimeStampFromFileName(file.Name) < nowTicks)
                .ToList();

            foreach (var file in staleFiles)
                await transaction.DeleteFileAsync(file.Name);

            await transaction.ChangeWorkingDirectoryAsync("../");
            log.Info($"Removed {staleFiles.Count} files from the '{directory}' folder in '{workingDirectory}'");
        }

        // There is an error in the Modified date of files so just calculate from timestamp
        private static long GetTimeStampFromFileName(string fileName)
        {
            if(!fileName.Contains('[') || !fileName.Contains(']'))
                throw new InvalidDataException("fileName doesnt contain a valid timestamp");
            var starterIndex = fileName.IndexOf("[", StringComparison.Ordinal);
            var closerIndex = fileName.IndexOf("]", StringComparison.Ordinal);
            var timeStampString = fileName.Substring(starterIndex +1, closerIndex -1);
            if (!long.TryParse(timeStampString, out var timeStamp))
                throw new InvalidDataException("fileName doesnt contain a valid timestamp");

            return timeStamp;
        }
    }
}
