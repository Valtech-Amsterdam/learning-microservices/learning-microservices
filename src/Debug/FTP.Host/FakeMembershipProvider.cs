﻿using FubarDev.FtpServer.AccountManagement;

namespace FTP.Host
{
    /// <summary>
    /// Allow any anonymous login.
    /// </summary>
    public sealed class FakeMembershipProvider : IMembershipProvider
    {
        /// <inheritdoc/>
        public MemberValidationResult ValidateUser(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
                return new MemberValidationResult(MemberValidationStatus.InvalidLogin);
            if (string.IsNullOrWhiteSpace(password))
                return new MemberValidationResult(MemberValidationStatus.InvalidLogin);

            return new MemberValidationResult(MemberValidationStatus.Anonymous, new AnonymousFtpUser(password));
        }
    }
}
