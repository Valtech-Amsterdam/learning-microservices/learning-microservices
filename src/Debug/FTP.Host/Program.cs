﻿using System;
using System.IO;
using FubarDev.FtpServer;
using FubarDev.FtpServer.AccountManagement;
using FubarDev.FtpServer.AccountManagement.Anonymous;
using FubarDev.FtpServer.CommandHandlers;
using FubarDev.FtpServer.FileSystem.DotNet;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace FTP.Host
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var services = new ServiceCollection();
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json", optional: true)
                .Build();

            services.Configure<DotNetFileSystemOptions>(opt => opt
                .RootPath = Path.Combine(Path.GetTempPath(), "TestFtpServer"));

            // Add FTP server services
            // DotNetFileSystemProvider = Use the .NET file system functionality
            // AnonymousMembershipProvider = allow only anonymous logins
            services.AddFtpServer(builder =>
            {
                builder
                    .UseDotNetFileSystem() // Use the .NET file system functionality
                    .EnableAnonymousAuthentication();

            });

            // Allow anonymous logins
            services.AddTransient<IMembershipProvider, FakeMembershipProvider>();
            services.AddTransient<IAnonymousPasswordValidator, NoValidation>();

            services.Configure<SystCommandOptions>(config);
            services.Configure<FtpServerOptions>(config);

            services.RemoveAll<FeatCommandHandler>();
            services.AddTransient<IFtpCommandHandler, CustomFeatCommandHandler>();

            using (var serviceProvider = services.BuildServiceProvider())
            {
                // Initialize the FTP server
                var ftpServer = serviceProvider.GetRequiredService<IFtpServer>();
                ftpServer.Start();

                // Start the FTP server

                Console.WriteLine("Press ENTER/RETURN to close the test application.");
                Console.ReadLine();

                // Stop the FTP server 
                ftpServer.Stop();
            }
        }
    }
}
